//Programa de echo.
char buf[20];
int cantChar;

void setup() {
  Serial.begin(9600); // opens serial port, sets data rate to 9600 bps
  Serial.setTimeout(10);
  pinMode(13, OUTPUT);
  digitalWrite(13, LOW);
}

void loop() {
  int i;
  
  cantChar = Serial.available();
  
  if( cantChar > 0 ){
    digitalWrite(13, HIGH);
          
    for(i = 0; i < cantChar; i++){
      buf[i] = Serial.read();
    }
    
    Serial.write(buf, cantChar);
    
    digitalWrite(13, LOW);
  }
}
