//Programa de echo.
char incomingByte;

void setup() {
  Serial.begin(9600); // opens serial port, sets data rate to 9600 bps
  Serial.setTimeout(10);
  pinMode(13, OUTPUT);
  digitalWrite(13, LOW);
}

void loop() {
  if(Serial.available() > 0 ){
    digitalWrite(13, HIGH);
    
    incomingByte = Serial.read();
    Serial.write(incomingByte);
    
    digitalWrite(13, LOW);
  }
}
