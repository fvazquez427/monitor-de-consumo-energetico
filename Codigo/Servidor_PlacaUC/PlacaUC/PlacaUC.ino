/*!
 * \file PlacaUC.ino
 * \date 16/12/2020
 * \version 1.0
 * \author Federico Alejandro Vazquez Saraullo
 * 
 * \brief Código de la placa microcontrolada que forma parte del servidor
 * \details Este código describe el funcionamiento de la placa Arduino UNO, la 
 *          cual forma parte del servidor. Esta placa se encarga de recibir 
 *          los pedidos de lectura de datos del servidor, de procesarlos, de 
 *          leer el módulo con el IC INA219 y de responderle al servidor.
 * 
 * \note Este código forma parte del proyecto final de la cátedra Técnicas 
 *       Digitales III, realizado por el grupo n°8 de la UTN-FRH durante el 
 *       ciclo lectivo 2020. 
 *       
 *       El código se probó exitosamente con una placa Arduino UNO.
 */
//Inclusión de bibliotecas.
#include <Wire.h>
#include <Adafruit_INA219.h>

//Macros.

/*!
 * \def BAUD_RATE
 * \brief Tasa de baudios del puerto de serie que se comunica con el servidor.
 */
#define BAUD_RATE 9600

/*!
 * \def MENSAJE_PEDIDO
 * \brief String que envía el servidor para pedir el estado actual.
 */
#define MENSAJE_PEDIDO "leer estado"

/*!
 * \def TAM_MENSAJE_PEDIDO
 * \brief Tamaño del string MENSAJE_PEDIDO.
 */
#define TAM_MENSAJE_PEDIDO sizeof(MENSAJE_PEDIDO)

/*!
 * \def TAM_RX_BUFFER
 * \brief Tamaño del buffer donde se guardan los caracteres enviados desde el 
 *        servidor.
 */
#define TAM_RX_BUFFER 20

/*!
 * \def TAM_TX_BUFFER
 * \brief Tamaño del buffer donde se guardan los caracteres enviados hacia el 
 *        servidor.
 */
#define TAM_TX_BUFFER 8

//Tipos de datos globales.

/*!
 * \struct estado_t
 * \brief Estado actual de corriente y tensión de bus de la carga.
 */
typedef struct{
  /*!
  * \var corriente_mA
  * \brief Corriente en mA de la carga.
  */
  float corriente_mA;

  /*!
  * \var corriente_mA
  * \brief Tensión del bus en V de la carga.
  */
  float tensionBus_V;
}estado_t;

/*!
 * \union conversionTxDatos_t
 * \brief Union que permite separar un dato estado_t en sus 8 bytes.
 */
typedef union{

  /*!
  * \var estado
  * \brief Estado actual de corriente y tensión de bus de la carga.
  */
  estado_t estado;

  /*!
  * \var estadoEnBytes[TAM_TX_BUFFER]
  * \brief Buffer que almacena los 8 bytes del estado actual.
  */
  char estadoEnBytes[TAM_TX_BUFFER];
}conversionTxDatos_t;

/*!
* \var ina219
* \brief Objeto global ina219.
*/
Adafruit_INA219 ina219;

/*!  
 *\fn void setup(void) 
 *
 *\brief Función de configuración de la placa microcontrolada.
 *
 *\details Esta función permite configurar el puerto serie y el INA219. 
 *         El puerto serie se inicia con 9600 Bd y 8N1. El INA219 se inicia
 *         con tensión de bus a plena escala de 32 V y corriente de shunt a 
 *         plena escala de 2 A, y luego se lo pasa a modo power-save.
 *         
 *\return void.
*/
void setup(void) 
{
  //Cuerpo de la función.
  Serial.begin(BAUD_RATE);
  
  ina219.begin(); 
  ina219.powerSave(true);
}

/*!  
 *\fn void loop(void) 
 *
 *\brief Función de configuración de la placa microcontrolada.
 *
 *\details Esta función permite leer los pedidos de estado del servidor y 
 *         responderle con la información requerida: tensión del bus en volts y 
 *         corriente en miliamperes.
 *         
 *\return void.
*/
void loop(void) 
{
  //Declaraciones locales.
  int i;
  int cantChar;
  char rxBuf[TAM_RX_BUFFER];
  char txBuf[TAM_TX_BUFFER];
  estado_t estadoActual;
  conversionTxDatos_t convTxDatos;

  //Cuerpo de la función.
  /*Se espera por pedidos de lectura desde la PC. Se lee de a un pedido. No 
   *se guarda el caracter nulo. */
  cantChar = Serial.available();
  
  if(cantChar >= TAM_MENSAJE_PEDIDO){
    //Se analiza que el pedido sea correcto.
    for(i = 0; i < TAM_MENSAJE_PEDIDO; i++){
      rxBuf[i] = Serial.read();
    }
    //Se agrega el caracter nulo.
    rxBuf[i] = '\n';

    if(strcmp(rxBuf, MENSAJE_PEDIDO) == 0){
      //Se sale del modo power-save.
      ina219.powerSave(false);

      //Se lee la tensión de bus y la corriente el INA219.
      estadoActual.tensionBus_V = ina219.getBusVoltage_V();
      estadoActual.corriente_mA = ina219.getCurrent_mA(); 

      //Se envía la información del estado a la PC.
      convTxDatos.estado = estadoActual;
      Serial.write(convTxDatos.estadoEnBytes, TAM_TX_BUFFER);
      
      //Se entra en el modo power-save.
      ina219.powerSave(true);
    }
  }
}
