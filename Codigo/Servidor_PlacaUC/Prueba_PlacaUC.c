// C library headers
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <fcntl.h> // Contains file controls like O_RDWR
#include <errno.h> // Error integer and strerror() function
#include <termios.h> // Contains POSIX terminal control definitions
#include <unistd.h> // write(), read(), close()
#include <sys/time.h>

#define TAM_TX_BUFFER 8

//Tipos de datos globales.
typedef struct{
  float corriente_mA;
  float tensionBus_V;
}estadoVI_t;

typedef struct{
  float corriente_mA;
  float tensionBus_V;
  float potencia_mW;
  float energia_Wh;
  float energia_J;
}estado_t;

typedef union{
  estadoVI_t estado;
  char estadoEnBytes[TAM_TX_BUFFER];
}conversionTxDatos_t;

//Prototipos de funciones.
void CalcularPotenciaYEnergia(estado_t *, struct timeval *);

//Función principal.
int main(int argc, char* argv[]) {
	/*Declaraciones locales*/
	int serial_port;
	conversionTxDatos_t convTxDatos;
	unsigned char msg[] = "leer estado";
	int num_bytes;
	estado_t estadoActual;
	struct timeval tiempoUltimaLectura;

	// Create new termios struc, we call it 'tty' for convention
	struct termios tty;

	/*Cuerpo de la función*/
	//Se valida que se ejecuta correcctamente por linea de comandos.
	if (argc != 2){
        fprintf(stderr,"\n\nERROR: Formato \"Nombre de puerto virtual\" \n\n");
        exit(1);
    }

	//Se abre el puerto serie virtual correspondiente.
	serial_port = open(argv[1], O_RDWR);

	// Read in existing settings, and handle any error
	if(tcgetattr(serial_port, &tty) != 0) {
	  printf("Error %i from tcgetattr: %s\n", errno, strerror(errno));
	  return 1;
	}

	tty.c_cflag &= ~PARENB; // Clear parity bit, disabling parity (most common)
	tty.c_cflag &= ~CSTOPB; // Clear stop field, only one stop bit used in communication (most common)
	tty.c_cflag &= ~CSIZE; // Clear all bits that set the data size
	tty.c_cflag |= CS8; // 8 bits per byte (most common)
	tty.c_cflag &= ~CRTSCTS; // Disable RTS/CTS hardware flow control (most common)
	tty.c_cflag |= CREAD | CLOCAL; // Turn on READ & ignore ctrl lines (CLOCAL = 1)

	tty.c_lflag &= ~ICANON;
	tty.c_lflag &= ~ECHO; // Disable echo
	tty.c_lflag &= ~ECHOE; // Disable erasure
	tty.c_lflag &= ~ECHONL; // Disable new-line echo
	tty.c_lflag &= ~ISIG; // Disable interpretation of INTR, QUIT and SUSP
	tty.c_iflag &= ~(IXON | IXOFF | IXANY); // Turn off s/w flow ctrl
	tty.c_iflag &= ~(IGNBRK|BRKINT|PARMRK|ISTRIP|INLCR|IGNCR|ICRNL); // Disable any special handling of received bytes

	tty.c_oflag &= ~OPOST; // Prevent special interpretation of output bytes (e.g. newline chars)
	tty.c_oflag &= ~ONLCR; // Prevent conversion of newline to carriage return/line feed
	// tty.c_oflag &= ~OXTABS; // Prevent conversion of tabs to spaces (NOT PRESENT ON LINUX)
	// tty.c_oflag &= ~ONOEOT; // Prevent removal of C-d chars (0x004) in output (NOT PRESENT ON LINUX)

	tty.c_cc[VTIME] = 10;    // Wait for up to 1s (10 deciseconds), returning as soon as any data is received.
	tty.c_cc[VMIN] = 0;

	// Set in/out baud rate to be 9600
	cfsetispeed(&tty, B9600);
	cfsetospeed(&tty, B9600);

	// Save tty settings, also checking for error
	if (tcsetattr(serial_port, TCSANOW, &tty) != 0) {
	  printf("Error %i from tcsetattr: %s\n", errno, strerror(errno));
	  return 1;
	}

	//Inicialización de tiempoUltimaLectura.
	gettimeofday(&tiempoUltimaLectura, NULL);

	//Inicialización de las energías del estadoActual.
	estadoActual.energia_Wh = 0;
	estadoActual.energia_J = 0;

	//Bucle de pedidos y lectura de información.
	while(1){
		// Write to serial port
		write(serial_port, msg, sizeof(msg));
		//printf("Mandando %ld bytes\n", sizeof(msg));
		// Normally you wouldn't do this memset() call, but since we will just receive
		// ASCII data for this example, we'll set everything to 0 so we can
		// call printf() easily.
		//memset(&read_buf, '\0', sizeof(read_buf));

		//Para demorar la lectura, recibir todo y demorar todo en gral.
		usleep(1000000); //Con 100 ms no se registra error con el módulo INA219.

		// Read bytes. The behaviour of read() (e.g. does it block?,
		// how long does it block for?) depends on the configuration
		// settings above, specifically VMIN and VTIME
		// n is the number of bytes read. n may be 0 if no bytes were received, and can also be -1 to signal an error.
		num_bytes = read(serial_port, &(convTxDatos.estadoEnBytes), 8);

		if(num_bytes == 0){
			usleep(1000);
		}
		else{
			if(num_bytes < 0){
				printf("Error en read: %s\n", strerror(errno));
				exit(2);
			}
			else{
				if(num_bytes !=  8){
					printf("Error: No se leyeron 8 bytes, sino %d\n", num_bytes);
					exit(3);
				}
				else{
					//Se obtiene todo el estado actual y se lo muestra.
					//Se copian los valores de corriente y tensión del bus.
					estadoActual.corriente_mA = convTxDatos.estado.corriente_mA;
					estadoActual.tensionBus_V = convTxDatos.estado.tensionBus_V;

					CalcularPotenciaYEnergia(&estadoActual, &tiempoUltimaLectura);
					system("clear");
					printf("\n\t\tCorriente (mA): %.3f"
						   "\n\t\tTension del bus (V): %.3f"
						   "\n\t\tPotencia (mW): %.3f"
						   "\n\t\tEnergia (Wh): %.3f"
						   "\n\t\tEnergia (J): %.3f\n",
					   		estadoActual.corriente_mA,
							estadoActual.tensionBus_V,
							estadoActual.potencia_mW,
							estadoActual.energia_Wh,
							estadoActual.energia_J);
				}
		  	}
		}
	}

	close(serial_port);
	return 0; // success
}

void CalcularPotenciaYEnergia(estado_t *pEstadoActual, struct timeval *pTiempoUltimaLectura)
{
	//Declaraciones locales.
	struct timeval tiempoActual;
	struct timeval difTiempo;
	unsigned long int difTiempo_us;
	float difTiempo_s;

	//Cuerpo de la función.
	//Se calcula la potencia.
	(pEstadoActual->potencia_mW) = (pEstadoActual->tensionBus_V) * (pEstadoActual->corriente_mA);

	//Se calcula la energía. Primero se obtiene el tiempo transcurrido en us.
	gettimeofday(&tiempoActual, NULL);

	difTiempo.tv_sec = tiempoActual.tv_sec - (pTiempoUltimaLectura->tv_sec);
	difTiempo.tv_usec = tiempoActual.tv_usec - (pTiempoUltimaLectura->tv_usec);
	//Se revierte el carry en caso de ser necesario para los us.
	if(difTiempo.tv_usec < 0){
		difTiempo.tv_sec --;
		difTiempo.tv_usec += 1000000;
	}

	difTiempo_us = (unsigned long int) difTiempo.tv_usec + \
	 			   (unsigned long int) (difTiempo.tv_sec * 1000000);
	difTiempo_s = (float) difTiempo_us / 1000000;

	//printf("difTiempo_us = %ld\ndifTiempo_s = %f\n", difTiempo_us, difTiempo_s);
	//getchar();

	//Se calcula finalmente la energia.
	(pEstadoActual->energia_Wh) += ((pEstadoActual->potencia_mW) / 1000) * (difTiempo_s / 3600);
	(pEstadoActual->energia_J) += ((pEstadoActual->potencia_mW) / 1000) * difTiempo_s;

	//Se actualiza el valor de tiempoUltimaLectura.
	(*pTiempoUltimaLectura) = tiempoActual;
}
