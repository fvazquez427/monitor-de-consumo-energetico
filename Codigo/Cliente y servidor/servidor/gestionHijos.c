/*!
*\addtogroup servidor
*@{
*/

/*!
*\file gestionHijos.c
*
*\date 03/02/2021
*\version 2.0
*\author Federico Alejandro Vazquez Saraullo
*
*\brief Archivo fuente que contiene las implementaciones de las funciones
*       para gestionar la base de datos (vector) de hijos.
*/

/*Inclusiones de bibliotecas*/
#include <stdio.h>     //printf y fprintf
#include <sys/types.h> //wait.
#include <sys/wait.h>  //wait.
#include <errno.h>     //errno.
#include <string.h>    //strerror
#include <signal.h>    //kill.

#include "servidor.h"     //Macro DEBUGGING y tipo de dato errorPrograma_t
#include "gestionHijos.h" //Por ser su archivo cabecera.

/*Desarrollo de funciones*/
void Handler_ReconocerHijoMuerto(int val)
{
  /*Declaraciones locales*/
  pid_t PIDHijo;
  int estadoWait;

  /*Cuerpo de la función*/
  PIDHijo = wait(&estadoWait);

  //Se reconoce al hijo muerto.
  if(PIDHijo == WAIT_ERROR){
    fprintf(stderr,"\nPADRE: ERROR al reconocer la muerte de un hijo "
            "con \"wait\": %s.\n", strerror(errno));
  }
  #ifdef DEBUGGING
  else{
    printf("\nPADRE: Se reconoce la muerte del hijo con PID %d y cuyo valor"
           " de fin corresponde con...\n\n\t", PIDHijo);

    /*Se analiza el valor retornado por exit del hijo con WEXITSTATUS solo si
     *la macro WIFEXITED(estadoWait) retorna verdadero.*/
    if(WIFEXITED(estadoWait)){
      switch((errorPrograma_t) (WEXITSTATUS(estadoWait))){
        case SIN_ERROR:
          printf("\"SIN ERRORES\".\n");
          break;

        case ERROR_LECTURA_CLIENTE_READ:
          printf("\"ERROR EN LECTURA DEL CLIENTE POR READ\".\n");
          break;

        case ERROR_LECTURA_CLIENTE_CANTIDAD_BYTES:
          printf("\"ERROR EN LECTURA DEL CLIENTE POR CANTIDAD "
                 "DE BYTES\".\n");
          break;

        case ERROR_ESCRITURA_CLIENTE_SEND:
          printf("\"ERROR EN ESCRITURA AL CLIENTE POR SEND\".\n");
          break;

        case ERROR_ESCRITURA_PUERTO_SERIE_WRITE:
          printf("\"ERROR EN ESCRITURA AL PUERTO SERIE POR WRITE\".\n");
          break;

        case ERROR_ESCRITURA_PUERTO_SERIE_CANTIDAD_BYTES:
          printf("\"ERROR EN ESCRITURA AL PUERTO SERIE POR CANTIDAD "
                 "DE BYTES\".\n");
          break;

        case ERROR_LECTURA_PUERTO_SERIE_READ:
          printf("\"ERROR EN LECTURA DEL PUERTO SERIE POR READ\".\n");
          break;

        case ERROR_LECTURA_PUERTO_SERIE_CANTIDAD_BYTES:
          printf("\"ERROR EN LECTURA DEL PUERTO SERIE POR CANTIDAD "
                 "DE BYTES\".\n");
          break;

        default :
          printf("\"ERROR NO REGISTRADO\".\n");
          break;
      }
    }
  }
  #endif //DEBUGGING
}

void InicializarVectorDeHijos(hijo_t vecHijos[], int cantHijos)
{
  //Declaraciones locales.
  int i;

  //Cuerpo de la función.
  for(i = 0; i < cantHijos; i++){
    vecHijos[i].estHijo = PID_HIJO_DISPONIBLE;
  }

  return;
}

int AgregarHijoAlVector(hijo_t vecHijos[], int cantHijos, pid_t PIDHijoNuevo)
{
  //Declaraciones locales.
  int i = 0;
  int agregadoBandera = -1; //Vale 0 si se lo pudo agregar y -1 si no.

  //Cuerpo de la función.
  /*Se busca un lugar disponible, si es que hay, y allí se coloca al nuevo
   *hijo.*/
  while(i < cantHijos && agregadoBandera == -1){
    if(vecHijos[i].estHijo == PID_HIJO_DISPONIBLE){
      vecHijos[i].estHijo = PID_HIJO_OCUPADO;
      vecHijos[i].hijoPID = PIDHijoNuevo;
      agregadoBandera = 0;
    }
    else{
      i++;
    }
  }

  return agregadoBandera;
}

int QuitarHijoDelVector(hijo_t vecHijos[], int cantHijos, pid_t PIDHijoAQuitar)
{
  //Declaraciones locales.
  int i = 0;
  int quitadoBandera = -1; //Vale 0 si se lo pudo sacar y -1 si no.

  //Cuerpo de la función.
  //Se busca el lugar donde está el hijo y, de existir, se lo quita.
  while(i < cantHijos && quitadoBandera == -1){
    if(vecHijos[i].hijoPID == PIDHijoAQuitar && \
       vecHijos[i].estHijo == PID_HIJO_OCUPADO){
      vecHijos[i].estHijo = PID_HIJO_DISPONIBLE;
      quitadoBandera = 0;
    }
    else{
      i++;
    }
  }

  return quitadoBandera;
}

int TerminarYSacarTodosLosHijos(hijo_t vecHijos[], int cantHijos)
{
  //Declaraciones locales.
  int i = 0;
  int errorBandera = 0; //Vale 0 si no hubo error y -1 si lo hubo.

  //Cuerpo de la función.
  /*Solo se deja un lugar del vector como disponible si no hubo problema al
   *enviar la señal SIGKILL.*/
  for(i = 0; i < cantHijos; i++){
    if(vecHijos[i].estHijo == PID_HIJO_OCUPADO){
      if(kill(vecHijos[i].hijoPID, SIGKILL) == KILL_ERROR){
        fprintf(stderr,"\nERROR al enviar la senial SIGKILL al hijo cuyo PID "
                "es %d: %s.\n", vecHijos[i].hijoPID, strerror(errno));
        errorBandera = -1;
      }
      else{
        vecHijos[i].estHijo = PID_HIJO_DISPONIBLE;
      }
    }
  }

  return errorBandera;
}
/*!
*}@
*/
