/*!
*\defgroup servidor Programa servidor
*\brief    Código del servidor.
*/

/*!
*\addtogroup servidor
*@{
*/

/*!
*\file servidor.c
*
*\date 03/02/2021
*\version 2.0
*\author Federico Alejandro Vazquez Saraullo
*
*\brief Archivo fuente que contiene la función principal "main" del servidor.
*
*\details El padre del servidor se comporta según el siguiente diagrama de
*         flujo: \image html "Servidor_Diagrama en bloques_Padre_v2.png"
*           Los hijos del servidor se comportan según los siguientes diagramas
*         de flujo: \image html "Servidor_Diagrama en bloques_Hijos_v2.png"
*
*\warning Por el momento solo se permite que el cliente pida una cosa a la vez:
*         o el estado o configurar las alertas.
*
*\warning La contraseña es sensible a mayúsculas y minúsculas.
*/

/*Inclusiones de bibliotecas.*/
#include <stdio.h>      //printf y fprintf.
#include <stdlib.h>     //exit.
#include <errno.h>      //errno.
#include <string.h>     //strerror y strcmp.
#include <strings.h>    //strcasecmp.
#include <netinet/in.h> //struct sockaddr_in y inet_ntoa.
#include <netinet/ip.h> //struct sockaddr_in.
#include <sys/types.h>  //select, pid_t, fork, kill, accept, send y getpid.
#include <sys/socket.h> //accept, inet_ntoa y send.
#include <sys/time.h>   //select.
#include <arpa/inet.h>  //inet_ntoa.
#include <unistd.h>  //select, pid_t, fork, usleep, read, close, getpid y write.
#include <signal.h>     //kill.
#include <time.h>       //struct timeval y gettimeofday.
#include <stddef.h>     //NULL.

#include "servidor.h"        //Por ser su archivo cabecera.
#include "gestionHijos.h"    //Handler_ReconocerHijoMuerto, etc.
#include "manejoStrings.h"   //LeerTexto.
#include "calculos.h"        //CalcularPotenciaYEnergia, etc.
#include "gestionRecursos.h" //IniciarServidor, etc.

/*Función principal*/
int main(int argc, char* argv[])
{
  /*Declaraciones locales*/
  //Para conexiones TCP.
  int serverTCPSocket; //socketfd del servidor.
  int newClientTCPSocket;  //socketfd para nueva conexión con el servidor.
  struct sockaddr_in clientTCPAddr;  //información del socket del cliente.
  int serverTCPAddrSize = sizeof(struct sockaddr_in);
  bandera_t excesoPedidosConexion = BANDERA_BAJA;
  int contEventosSelect = 0;
  int contadorConexionesActivas = 0;

  //Para manejar el puerto serie virtual.
  int serialPort;

  //Para los select.
  fd_set readFDsPadre;

  //Para comunicarse entre padre e hijos.
  int pipeFd[2];
  pid_t PIDHijoAQuitar;
  pid_t PIDHijoAAgregar;

  //Para la base de datos del padre sobre los hijos.
  hijo_t vecHijos[CANT_MAX_HIJOS];

  //Para lectura del teclado.
  char buffer [STDIN_BUFFER_SIZE];
  bandera_t finalizarPrograma = BANDERA_BAJA;

  //Para control de errores del programa.
  errorPrograma_t estadoPrograma = SIN_ERROR;

  //Para comunicarse entre hijos (memoria compartida + semáforo).
  paqueteServidor_t *pMemComp;
  int semId;

  /*Cuerpo de la función*/

  /*Se inicializa el servidor. De haber un error, se termina el programa.*/
  printf("\nINICIANDO servidor...\n");
  estadoPrograma = IniciarServidor(argc, argv, &serverTCPSocket, pipeFd,
                                   &pMemComp, &semId, &serialPort, vecHijos,
                                   CANT_MAX_HIJOS);

  if(estadoPrograma != SIN_ERROR){
    printf("\n\n\t\tFIN DEL PROGRAMA\n\n");
    exit((int) estadoPrograma);
  }

  /*Se crea un hijo encargado de actualizar el estado eléctrico y de las
  *alertas. Si hay algún problema, termina el programa.*/
  PIDHijoAAgregar = fork();
  if(PIDHijoAAgregar == ERROR_CREAR_HIJO){
    fprintf(stderr, "\nERROR al crear el primer hijo: %s.\n\n\t\tFIN DEL "
            "PROGRAMA\n\n", strerror(errno));
    estadoPrograma = ERROR_FORK;
    exit((int) estadoPrograma);
  }
  else if (!PIDHijoAAgregar){
    /*Declaraciones locales del hijo.*/
    //Para comunicación con el puerto serie.
    conversionRxDatos_t convRxDatos;
    unsigned char msg[] = "leer estado";
    int cantSerieBytes;
    paqueteServidor_t paqueteServidor;

    //Para cálculo del estado.
    struct timeval tiempoUltimaLectura;

    /*Cuerpo del hijo.*/
    //Inicialización de estadoPrograma del hijo por las dudas.
    estadoPrograma = SIN_ERROR;

    //Inicialización de las energías del estado actual.
    paqueteServidor.estad.energia_Wh = 0;
    paqueteServidor.estad.energia_J = 0;

    /*Se obtiene el tiempo actual como referencia. Si hubo un error, se muestra
     *un mensaje por pantalla.*/
    if(gettimeofday(&tiempoUltimaLectura, NULL) == ERROR_GET_TIME){
      fprintf(stderr, "\nERROR al obtener el tiempo actual con "
              "gettimeofday: %s.\n\n", strerror(errno));
      estadoPrograma = ERROR_GET_TIME_OF_DAY;
    }

    /*Bucle principal: Se actualiza constantemente el estado eléctrico y de las
     *alertas.*/
    while(1){
      //Se envía un pedido de lectura a la placa microcontrolada.
      cantSerieBytes = write(serialPort, msg, sizeof(msg));

      /*Se muestra un mensaje por pantalla en caso de error en la
       *escritura del puerto serie virtual.*/
      if(cantSerieBytes < 0){
        fprintf(stderr, "\nERROR al escribir el puerto serie virtual: %s.\n",
                strerror(errno));
        estadoPrograma = ERROR_ESCRITURA_PUERTO_SERIE_WRITE;
      }
      else if(cantSerieBytes !=  sizeof(msg)){
        printf("\n\nERROR al escribir el puerto serie virtual: "
               "No se escribieron %ld bytes, sino %d.\n\n",
               sizeof(msg), cantSerieBytes);
        estadoPrograma = ERROR_ESCRITURA_PUERTO_SERIE_CANTIDAD_BYTES;
      }
      else{
        //Para demorar la lectura y recibir todo correctamente.
        usleep(SERIAL_PORT_DELAY_USEC);

        /*Se lee la respuesta de la placa microcontrolada y se
         *verifica que sea correcta.*/
        cantSerieBytes = read(serialPort, &(convRxDatos.estadoEnBytes),
                              TAM_SERIAL_PORT_RX_BUFFER);

        if(cantSerieBytes < 0){
          fprintf(stderr, "\nERROR al leer el puerto serie virtual: %s.\n",
                  strerror(errno));
          estadoPrograma = ERROR_LECTURA_PUERTO_SERIE_READ;
        }
        else if(cantSerieBytes !=  TAM_SERIAL_PORT_RX_BUFFER){
          printf("\n\nERROR al leer el puerto serie virtual: No se "
                 "leyeron 8 bytes, sino %d.\n\n", cantSerieBytes);
          estadoPrograma = ERROR_LECTURA_PUERTO_SERIE_CANTIDAD_BYTES;
        }
        else{
          //Se obtiene todo el estado actual.
          paqueteServidor.estad.corriente_mA = convRxDatos.estado.corriente_mA;
          paqueteServidor.estad.tensionBus_V = convRxDatos.estado.tensionBus_V;
          CalcularPotenciaYEnergia(&(paqueteServidor.estad), &tiempoUltimaLectura);

          //Se analiza si se activa alguna alarma.
          AnalizarAlertas(&paqueteServidor, semId, pMemComp);

          /*Se actualiza la información del estado eléctrico y de las alertas
           *en la memoria compartida. El valor de las alertas solo son
           *modificados por los otros hijos.*/
          ActualizarMemoria(paqueteServidor, semId, pMemComp);
        }
      }
    }
  }

  /*Se agrega al primer hijo al vector de hijos. Si hay un error, se mata a
   *dicho hijo, se muestra un mensaje por pantalla y se termina la ejecución.*/
  if(AgregarHijoAlVector(vecHijos, CANT_MAX_HIJOS,
                         PIDHijoAAgregar) == ERROR_AGREGAR_HIJO){
    fprintf(stderr, "\n\nERROR al agregar al primer hijo, con PID %d, en el "
            "vector.\n\n", PIDHijoAAgregar);
    if(kill(PIDHijoAAgregar, SIGKILL) == KILL_ERROR){
      fprintf(stderr,"\nERROR al enviar la senial SIGKILL al primer hijo, cuyo "
              "PID es %d: %s.\n", PIDHijoAAgregar, strerror(errno));
    }
    printf("\n\n\t\tFIN DEL PROGRAMA\n\n");
    estadoPrograma = ERROR_METER_HIJO_AL_VECTOR;
    exit((int) estadoPrograma);
  }

  //El servidor se inició correctamente.
  printf("\n\tServidor iniciado CORRECTAMENTE.\n\nEscriba \"fin\" y presione "
         "enter cuando quiera finalizar el programa.\n");

  //Se ejecuta el programa hasta que se reciba "fin" del teclado.
  while(finalizarPrograma == BANDERA_BAJA){
    //Configuración del reading fd set.
    FD_ZERO(&readFDsPadre);
    FD_SET(STDIN_FD, &readFDsPadre);

    if(excesoPedidosConexion == BANDERA_BAJA){
      FD_SET(serverTCPSocket, &readFDsPadre);
    }

    FD_SET(pipeFd[0], &readFDsPadre);

    #ifdef DEBUGGING
    printf("\nPADRE: Entra al select.\n");
    #endif //DEBUGGING
    /*Se espera con select según cuál sea el fd más grande. Se espera por
     *ingreso de teclado, un pedido de conexión y/o un mensaje por el
     *pipe.*/
    if((excesoPedidosConexion == BANDERA_BAJA) && (serverTCPSocket > pipeFd[0])){
      select(serverTCPSocket + 1, &readFDsPadre, NULL, NULL, NULL);
    }
    else{
      select(pipeFd[0] + 1, &readFDsPadre, NULL, NULL, NULL);
    }
    #ifdef DEBUGGING
    printf("\nPADRE: Sale del select.\n");
    #endif //DEBUGGING

    //Se analiza si se escribió "fin" por teclado.
    if (FD_ISSET(STDIN_FD, &readFDsPadre)){
      #ifdef DEBUGGING
      printf("\nPADRE: Entra a STDIN por select.\n");
      #endif //DEBUGGING
      if(contEventosSelect > 0){
        contEventosSelect--;
      }
      else{
        #ifdef DEBUGGING
        printf("\nPADRE: Entra a LeerTexto por STDIN.\n");
        #endif //DEBUGGING

        LeerTexto(buffer, STDIN_BUFFER_SIZE);

        #ifdef DEBUGGING
        printf("\nPADRE: Sale de LeerTexto.\n");
        #endif //DEBUGGING

        /*Se comparo el texto recibido con "fin". De ser iguales, se
         *terminan todos los procesos.*/
        if(!strcasecmp(buffer, "fin")){
          finalizarPrograma = BANDERA_ALTA;
          TerminarYSacarTodosLosHijos(vecHijos, CANT_MAX_HIJOS);
        }
      }
    }

    /*Se analiza si se recibió algo por el pipe. En caso afirmativo, se
     *intenta quitar el hijo correspondiente del vector. Se supone que no se
     *puede presentar más de un mensaje por el pipe cuando se sale del select
     *por dicho IPC.*/
    if (FD_ISSET(pipeFd[0], &readFDsPadre)){
      #ifdef DEBUGGING
      printf("\nPADRE: Entra a PIPE por select.\n");
      #endif //DEBUGGING

      if(contEventosSelect > 0){
        contEventosSelect--;
      }
      else{
        #ifdef DEBUGGING
        printf("\nPADRE: Lee del PIPE.\n");
        #endif //DEBUGGING

        read(pipeFd[0], &PIDHijoAQuitar, sizeof(PIDHijoAQuitar));

        //Para evitar eventos por STDIN, el pipe y el socket al morir el hijo.
        if(excesoPedidosConexion == BANDERA_BAJA){
          contEventosSelect = 3;
        }

        /*Se baja la bandera excesoPedidosConexion porque se terminó una
        conexión.*/
        excesoPedidosConexion = BANDERA_BAJA;

        //Se decrementa el contador de conexiones activas.
        contadorConexionesActivas--;

        #ifdef DEBUGGING
        printf("\nPADRE: Se quita al hijo con PID %d del vector.\n",
               PIDHijoAQuitar);
        #endif //DEBUGGING

        if(QuitarHijoDelVector(vecHijos, CANT_MAX_HIJOS,
                               PIDHijoAQuitar) == ERROR_QUITAR_HIJO){
          printf("\n\nERROR al quitar el hijo de PID %d del vector.\n\n",
                 PIDHijoAQuitar);
        }
      }
    }

    /*Se analiza si se recibió ahora o antes algún pedido de conexión por el
     *socket TCP. En caso afirmativo, se acepta dicho pedido siempre que no
     *se tengan ya CANT_MAX_CONEXIONES_CONCURRENTES conexiones activas.*/
    if (FD_ISSET(serverTCPSocket, &readFDsPadre) && \
        excesoPedidosConexion == BANDERA_BAJA){
      #ifdef DEBUGGING
      printf("\nPADRE: Entra por SOCKET del select.\n");
      #endif //DEBUGGING

      if(contEventosSelect > 0){
        contEventosSelect--;
      }
      else{
        #ifdef DEBUGGING
        printf("\nPADRE: Entra por SOCKET y NO por contEventosSelect.\n");
        #endif //DEBUGGING

        if(contadorConexionesActivas < CANT_MAX_CONEXIONES_CONCURRENTES){
          //Se intenta aceptar el pedido de conexión.
          printf("\nAceptando una conexion...\n");
          newClientTCPSocket = accept(serverTCPSocket,
                                     (struct sockaddr *)&clientTCPAddr,
                                     &serverTCPAddrSize);

          if(newClientTCPSocket == ERROR_ACEPTAR_CONEXION){
            fprintf(stderr,"\n\tERROR al aceptar una conexion TCP: %s.\n",
                    strerror(errno));
          }
          else{
            //Mensaje por pantalla para saber que cliente se conectó.
            printf("\n\tSe acepta una conexion con el cliente de IP %s.\n",
                  (char *) inet_ntoa(clientTCPAddr.sin_addr));

            //Se crea un hijo que atienda cada conexión.
            PIDHijoAAgregar = fork();
            if (!PIDHijoAAgregar){
              /*Declaraciones locales del hijo.*/
              //Para la comunicación TCP.
              int numRxTCPBytes;
              pedidoDelCliente_t pedido;
              bandera_t clienteDesconectado = BANDERA_BAJA;
              mensajeDelServidor_t mensajeServidor;
              alertas_t alertasConfiguradas;

              //Para el select.
              fd_set readFDsHijo;
              struct timeval tvHijo;
              int cntTimeOutSeguidos = 0;

              /*Cuerpo del hijo.*/
              //Inicialización de estadoPrograma del hijo por las dudas.
              estadoPrograma = SIN_ERROR;

              /*Se leen pedidos por TCP hasta que pase TIMEOUT_MAX_HIJO_SEC sin
               *pedidos o el cliente se desconecte.*/
              while(cntTimeOutSeguidos < MAX_CANT_TIMEOUT_HIJO && \
                    clienteDesconectado == BANDERA_BAJA){
                /*Seteo timeout en TIMEOUT_HIJO_SEC segundos y TIMEOUT_HIJO_USEC
                 *microsegundos*/
                tvHijo.tv_sec = TIMEOUT_HIJO_SEC;
                tvHijo.tv_usec = TIMEOUT_HIJO_USEC;

                //Configuración del reading fd set.
                FD_ZERO(&readFDsHijo);
                FD_SET(newClientTCPSocket, &readFDsHijo);

                //Se espera con select un pedido del cliente por TCP.
                select(newClientTCPSocket + 1, &readFDsHijo, NULL, NULL,
                       &tvHijo);

                /*Se analiza si se recibió algún pedido del cliente por TCP. Si
                 *efectivamente se recibió, se lo procesa y se resetea el
                 *contador cntTimeOutSeguidos. Por el contrario, si no se recive
                 *ningún pedido significa que salió por timeout, por lo que se
                 *incrementa el contador cntTimeOutSeguidos.*/
                if(FD_ISSET(newClientTCPSocket, &readFDsHijo)){
                  cntTimeOutSeguidos = 0;

                  //Se lee el pedido del cliente por TCP.
                  numRxTCPBytes = read(newClientTCPSocket, &(pedido),
                                      sizeof(pedido));

                  if(numRxTCPBytes < 0){
                    fprintf(stderr, "\nERROR al leer el pedido del cliente "
                           "del socket TCP: %s.\n", strerror(errno));

                    estadoPrograma = ERROR_LECTURA_CLIENTE_READ;
                  }
                  else{
                    /*Cuando el cliente se desconecta o termina subitamente,
                     *suceden interminables eventos en el socket TCP que hacen
                     *que se salga del select. Particularmente, se reciben 0
                     *bytes cuando eso sucede, por lo que se toma esta condición
                     *como la que determina que el cliente se desconectó. */
                    if(numRxTCPBytes == 0){
                      clienteDesconectado = BANDERA_ALTA; //Para terminar.
                    }
                    else{
                      if(numRxTCPBytes != sizeof(pedido)){
                        printf("\n\nERROR al leer el pedido del cliente del "
                        "socket TCP: No se leyeron %ld bytes, sino %d.\n\n",
                        sizeof(pedido), numRxTCPBytes);
                        estadoPrograma = ERROR_LECTURA_CLIENTE_CANTIDAD_BYTES;
                      }
                      else{
                        /*\bug Por el momento solo se permite que el cliente
                         *     pida una cosa a la vez.*/
                        if( (pedido.pedEstado == PEDIR_ESTADO_SI) && \
                            (pedido.pedAlertas == CAMBIAR_ALERTAS_NO)){
                          //Se envía el estado actual al cliente por TCP.
                          EnviarEstadoAlCliente(newClientTCPSocket, semId,
                                                pMemComp, &estadoPrograma);
                        }
                        else if( (pedido.pedEstado == PEDIR_ESTADO_NO) && \
                                 (pedido.pedAlertas == CAMBIAR_ALERTAS_SI)){
                          //Se envía el pedido de contraseña al cliente por TCP.
                          mensajeServidor = SERVIDOR_PIDE_CONTRASENIA;
                          if(send(newClientTCPSocket, &mensajeServidor,
                                  sizeof(mensajeServidor),
                                  0) == ERROR_SEND_TCP){
                            fprintf(stderr, "\nERROR al enviar el pedido de "
                                   "contrasenia al cliente por TCP: %s.\n",
                                   strerror(errno));
                            estadoPrograma = ERROR_ESCRITURA_CLIENTE_SEND;
                          }
                          else{
                            /*Se lee la contraseña enviada por el cliente. Lo
                             *más probable es que se lean menos de
                             *sizeof(buffer) bytes, por eso no se verifica si
                             *numRxTCPBytes < sizeof(buffer).*/
                            numRxTCPBytes = read(newClientTCPSocket, buffer,
                                                 sizeof(buffer));

                            if(numRxTCPBytes < 0){
                              fprintf(stderr, "\nERROR al leer la contrasenia "
                                      "del cliente por el socket TCP: %s.\n",
                                      strerror(errno));

                              estadoPrograma = ERROR_LECTURA_CLIENTE_READ;
                            }
                            //Se analiza si se desconectó el cliente.
                            else if(numRxTCPBytes == 0){
                              clienteDesconectado = BANDERA_ALTA;
                            }
                            /*Se analiza si la contraseña es incorrecta. Si lo
                             *es, se envía un aviso de esto al cliente.*/
                            else if(strcmp(buffer, CONTRASENIA)){
                              mensajeServidor = SERVIDOR_AVISA_CONTRASENIA_INCORRECTA;
                              if(send(newClientTCPSocket, &mensajeServidor,
                                      sizeof(mensajeServidor),
                                      0) == ERROR_SEND_TCP){
                                fprintf(stderr, "\nERROR al enviar el aviso de "
                                       "contrasenia incorrecta al cliente por "
                                       "TCP: %s.\n", strerror(errno));
                                estadoPrograma = ERROR_ESCRITURA_CLIENTE_SEND;
                              }
                            }
                            /*Si la contraseña es correcta, se envía un pedido
                             *de información de configuración.*/
                            else{
                              mensajeServidor = SERVIDOR_PIDE_CONFIGURACION;
                              if(send(newClientTCPSocket, &mensajeServidor,
                                      sizeof(mensajeServidor),
                                      0) == ERROR_SEND_TCP){
                                fprintf(stderr, "\nERROR al enviar el pedido "
                                       "de informacion de configuracion al "
                                       "cliente por TCP: %s.\n", strerror(errno));
                                estadoPrograma = ERROR_ESCRITURA_CLIENTE_SEND;
                              }
                              else{
                                /*Se lee la información de configuración enviada
                                 *por el cliente mediante TCP.*/
                                numRxTCPBytes = read(newClientTCPSocket,
                                                     &alertasConfiguradas,
                                                     sizeof(alertasConfiguradas));

                                if(numRxTCPBytes < 0){
                                  fprintf(stderr, "\nERROR al leer la "
                                         "informacion de configuracion enviada "
                                         "por el cliente mediante el socket "
                                         "TCP: %s.\n", strerror(errno));

                                  estadoPrograma = ERROR_LECTURA_CLIENTE_READ;
                                }
                                //Se analiza si se desconectó el cliente.
                                else if(numRxTCPBytes == 0){
                                    clienteDesconectado = BANDERA_ALTA;
                                }
                                //Se analizan la cantidad de bytes recibidos.
                                else if(numRxTCPBytes != sizeof(alertasConfiguradas)){
                                  printf("\n\nERROR al leer la informacion de "
                                  "configuracion enviada por el cliente "
                                  "mediante el socket TCP: No se leyeron %ld "
                                  "bytes, sino %d.\n\n",
                                  sizeof(alertasConfiguradas), numRxTCPBytes);
                                  estadoPrograma = ERROR_LECTURA_CLIENTE_CANTIDAD_BYTES;
                                }
                                else{
                                  /*Se envía un mensaje al cliente confirmando
                                   *que se hizo la configuración.*/
                                   mensajeServidor = SERVIDOR_AVISA_CONFIGURACION_HECHA;
                                   if(send(newClientTCPSocket, &mensajeServidor,
                                           sizeof(mensajeServidor),
                                           0) == ERROR_SEND_TCP){
                                     fprintf(stderr, "\nERROR al enviar la "
                                            "confirmacion de configuracion al "
                                            "cliente por TCP: %s.\n",
                                            strerror(errno));
                                     estadoPrograma = ERROR_ESCRITURA_CLIENTE_SEND;
                                   }

                                  //Se actualizan los valores de las alertas.
                                  ActualizarAlertas(alertasConfiguradas, semId,
                                                    pMemComp);
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
                else{
                  cntTimeOutSeguidos++;
                }
              }

              /*Mensaje por pantalla para saber que cliente se desconectó o no
               *envió más pedidos.*/
              if(cntTimeOutSeguidos == MAX_CANT_TIMEOUT_HIJO){
                printf("\nSe termina la conexion con el cliente de IP %s "
                       "porque este no envio mas pedidos (keepalive).\n",
                      (char *) inet_ntoa(clientTCPAddr.sin_addr));
              }
              if(clienteDesconectado == BANDERA_ALTA){
                printf("\nSe termina la conexion con el cliente de IP %s "
                       "porque este se desconecto.\n",
                      (char *) inet_ntoa(clientTCPAddr.sin_addr));
              }

              /*Se cierra el socket y se avisa al padre por el pipe que este
               *hijo terminó su ejecución, retornando por exit el valor del
               *último error ocurrido.*/
              close(newClientTCPSocket);
              PIDHijoAQuitar = getpid();
              write(pipeFd[1], &PIDHijoAQuitar, sizeof(PIDHijoAQuitar));
              exit((int) estadoPrograma);
            }
            else{
              /*El padre agrega el hijo al vector, incrementa el contador de
               *conexiones activas y cierra el socket de la reciente conexión
               *porque no la necesita.*/
              if(AgregarHijoAlVector(vecHijos, CANT_MAX_HIJOS,
                                     PIDHijoAAgregar) == ERROR_AGREGAR_HIJO){
                printf("\n\nERROR al agregar el hijo de PID %d en el vector.\n\n",
                       PIDHijoAAgregar);
              }
              else{
                contadorConexionesActivas++;
              }
              close(newClientTCPSocket);
            }
          }
        }
        else{
          //Se sube la bandera excesoPedidosConexion.
          #ifdef DEBUGGING
          printf("\nPADRE: No se acepta la conexion porque se excedio la "
                 "cantidad maxima simultanea.\n");
          #endif //DEBUGGING

          excesoPedidosConexion = BANDERA_ALTA;
        }
      }
    }
  }

  /*Se cierra el socket principal TCP del servidor, el pipe y el puerto serie
   *virtual.*/
  TerminarServidor(serverTCPSocket, pipeFd, serialPort);

  //Fin del programa.
  printf("\n\n\t\tFIN DEL PROGRAMA\n\n");
  return 0;
}
/*!
*}@
*/
