/*!
*\addtogroup servidor
*@{
*/

/*!
*\file gestionRecursos.c
*
*\date 03/02/2021
*\version 2.0
*\author Federico Alejandro Vazquez Saraullo
*
*\brief Archivo fuente que contiene las implementaciones de las funciones
*       para gestionar los recursos del servidor.
*/

/*Inclusiones de bibliotecas*/
#include <stdio.h>      //fprintf.
#include <stddef.h>     //NULL.
#include <errno.h>      //errno.
#include <string.h>     //strerror.
#include <termios.h>    //Puerto serie virtual.
#include <unistd.h>     //close, struct termios, pipe y sleep.
#include <termios.h>    //struct termios.
#include <signal.h>     //signal.
#include <sys/time.h>   //struct timeval y gettimeofday.
#include <netinet/in.h> //struct sockaddr_in.
#include <sys/types.h>  //socket, bind, listen, open, shmat
#include <sys/socket.h> //socket, bind, listen
#include <sys/ipc.h>    //shmget, semget.
#include <sys/shm.h>    //shmget, shmat.
#include <sys/sem.h>    //semget.
#include <arpa/inet.h>  //htons.
#include <sys/stat.h>   //open.
#include <fcntl.h>      //open.

#include "gestionRecursos.h" //Por ser su archivo cabecera.

/*Desarrollo de funciones*/
errorPrograma_t IniciarServidor(
  int argc,
  char *argv[],
  int *pServerTCPSocket,
  int pipeFd[],
  paqueteServidor_t **ppMemComp,
  int *pSemId,
  int *pSerialPort,
  hijo_t vecHijos[],
  int cantHijos)
{
  /*Declaraciones locales*/
  //Para guardar la información del socket del servidor.
  struct sockaddr_in serverTCPAddr;

  //Para inicar el puerto serie virtual.
  struct termios tty;

  //Para control de errores del programa.
  errorPrograma_t estadoPrograma = SIN_ERROR;

  //Para la memoria compartida.
  key_t memCompKey;
  int memCompId;

  //Para el semáforo.
  union semun semArg;

  /*Cuerpo de la función*/
  //Se valida que se ejecuta correctamente por línea de comandos.
  if (argc != CANT_MAIN_ARG){
    fprintf(stderr, "\n\nERROR: No se cumple con el formato de ejecucion "
            "\"./Nombre_Ejecutable Nombre_Del_Puerto_Virtual\"\n\n");
    estadoPrograma = ERROR_ARGUMENTOS_MAIN;
  }
  /*Configuración del socket TCP.*/
  //Se crea un socket TCP y verifica si hubo algún error.
  else if (((*pServerTCPSocket) = socket(AF_INET, SOCK_STREAM, 0)) \
            == SOCKET_ERROR){
    fprintf(stderr, "\nERROR al crear el socket TCP del servidor: %s.\n\n"
            , strerror(errno));
    estadoPrograma = ERROR_CREAR_SOCKET;
  }
  else{
    /*Se carga la estructura serverTCPAddr para luego poder llamar a la
    *función bind(). Se configura para TCP, el puerto SERVIDOR_PUERTO_TCP y la
    *IP local de la PC.*/
    serverTCPAddr.sin_family = AF_INET;
    serverTCPAddr.sin_port = htons(SERVIDOR_PUERTO_TCP);
    serverTCPAddr.sin_addr.s_addr = INADDR_ANY;
    bzero(&(serverTCPAddr.sin_zero), 8);

    if(bind((*pServerTCPSocket), (struct sockaddr *)&serverTCPAddr,
           sizeof(struct sockaddr)) == BIND_ERROR){
      fprintf(stderr, "\nERROR en TCP bind del servidor: %s.\n\n",
              strerror(errno));
      estadoPrograma = ERROR_HACER_BIND;
    }
    /*Se habilita el socket para recibir conexiones, con una cola de
    CANT_MAX_CONEXIONES_ENCOLADAS conexiones en espera como máximo */
    else if (listen((*pServerTCPSocket), CANT_MAX_CONEXIONES_ENCOLADAS) \
                   == LISTEN_ERROR){
    fprintf(stderr, "\nERROR en TCP listen del servidor: %s.\n\n",
            strerror(errno));
      estadoPrograma = ERROR_HACER_LISTEN;
    }
    /*Creación del pipe para que los hijos le avisen al padre cuando terminan
     *su ejecución.*/
    else if (pipe(pipeFd) == PIPE_ERROR){
      fprintf(stderr, "\nERROR al crear el pipe del servidor: %s.\n\n",
              strerror(errno));
      estadoPrograma = ERROR_CREAR_PIPE;
    }
    /*Configuración de los handlers de señales.*/
    //Se configura el handler para atender la finalización de un hijo.
    else if(signal(SIGCHLD, Handler_ReconocerHijoMuerto) == SIG_ERR){
      fprintf(stderr, "\nERROR al asociar SIGCHLD con handler propio: %s.\n\n",
              strerror(errno));
      estadoPrograma = ERROR_SIGNAL_SIGCHLD;
    }
    else{
      /*Configuración del puerto serie virtual.*/
      //Se abre el puerto serie virtual correspondiente para leer y escribir.
      (*pSerialPort) = open(argv[1], O_RDWR);
      if((*pSerialPort) == SERIAL_PORT_ERROR){
        fprintf(stderr, "\nERROR %i por el puerto serie virtual al hacer "
                "open: %s.\n\n", errno, strerror(errno));
        estadoPrograma = ERROR_OPEN_PUERTO_SERIE;
      }
      //Se lee la configuración actual del puerto serie virtual.
      else if(tcgetattr((*pSerialPort), &tty) != SERIAL_PORT_OK) {
        fprintf(stderr, "\nERROR %i por el puerto serie virtual "
               "al hacer tcgetattr: %s.\n\n", errno, strerror(errno));
        estadoPrograma = ERROR_TCGETATTR_PUERTO_SERIE;
      }
      else{
        //Se configura el puerto serie virtual.
        tty.c_cflag &= ~PARENB;        //Sin paridad.
        tty.c_cflag &= ~CSTOPB;        //1 solo bit de fin.
        tty.c_cflag &= ~CSIZE; 	       //Pone en 0 todos los bits de "data size".
        tty.c_cflag |= CS8; 	         //8 bits de datos.
        tty.c_cflag &= ~CRTSCTS;       //Deshabilita RTS/CTS.
        tty.c_cflag |= CREAD | CLOCAL; /*Enciende READ e ignora la líneas de
                                         control.*/
        tty.c_lflag &= ~ICANON;
        tty.c_lflag &= ~ECHO; 	       //Deshabilita echo.
        tty.c_lflag &= ~ECHOE; 	       //Deshabilita erasure.
        tty.c_lflag &= ~ECHONL;        //Deshabilita new-line echo.
        tty.c_lflag &= ~ISIG; //Deshabilita interpretación de INTR, QUIT y SUSP.
        tty.c_iflag &= ~(IXON | IXOFF | IXANY); //Apaga control de flujo s/w.

        //Deshabilita cualquier manejo especial de los datos recibidos.
        tty.c_iflag &= ~(IGNBRK|BRKINT|PARMRK|ISTRIP|INLCR|IGNCR|ICRNL);

        /*Se previene la interpretación especial de bytes de salida (e.g.
         *newline chars)*/
        tty.c_oflag &= ~OPOST;

        //Se previene la conversión de newline to carriage return/line feed.
        tty.c_oflag &= ~ONLCR;

        /*Se configura time-out de read en TIMEOUT_SERIAL_PORT_DSEC
         *decisegundos.*/
        tty.c_cc[VTIME] = TIMEOUT_SERIAL_PORT_DSEC;
        tty.c_cc[VMIN] = 0;

        //Configuración de tasa de baudios en 9600 Bd para lectura y escritura.
        cfsetispeed(&tty, B9600);
        cfsetospeed(&tty, B9600);

        //Se guarda efectivamente la configuración del puerto serie virtual.
        if (tcsetattr((*pSerialPort), TCSANOW, &tty) != SERIAL_PORT_OK) {
          fprintf(stderr, "\nERROR %i por el puerto serie virtual "
                 "al hacer tcsetattr: %s.\n\n", errno, strerror(errno));
          estadoPrograma = ERROR_TCGETATTR_PUERTO_SERIE;
        }
        else{
          /*Se esperan SERIAL_PORT_DELAY_SEC segundos para que se termine de
           *inicializar el puerto serie virtual.*/
          sleep(SERIAL_PORT_DELAY_SEC);

          //Se inicializa el vector de hijos.
          InicializarVectorDeHijos(vecHijos, cantHijos);

          /*Inicio y configuración de la memoria compartida.*/
          //Se genera la key para la memoria compartida y el semáforo.
          if ((memCompKey = ftok("gestionRecursos.c", 'A')) == ERROR_FTOK){
            fprintf(stderr, "\nERROR al generar la key de la memoria "
                    "compartida: %s.\n\n", strerror(errno));
            estadoPrograma = ERROR_KEY_FTOK;
          }
          //Se abre la memoria compartida.
          else if ((memCompId = shmget(memCompKey, TAM_MEMORIA_COMPARTIDA,
                                       0644 | IPC_CREAT)) == ERROR_SHMGET){
            fprintf(stderr, "\nERROR al abrir la memoria compartida: %s.\n\n",
                    strerror(errno));
            estadoPrograma = ERROR_MEM_COMP_SHMGET;
          }
          else{
            /*Se adjunta a la memoria compartida y verifica que el puntero sea
             *válido.*/
            (*ppMemComp) = (paqueteServidor_t*) shmat(memCompId, NULL, 0);

            if( (*ppMemComp) == (paqueteServidor_t*)(ERROR_SHMAT)){
              fprintf(stderr, "\nERROR al adjuntar la memoria compartida: "
                      "%s.\n\n", strerror(errno));
              estadoPrograma = ERROR_MEM_COMP_SHMAT;
            }
            else{
              //Se inicializa la memoria compartida.
              (**ppMemComp).alert.alMaxI.alertaMaxIVal = MAX_I_DEFECTO;
              (**ppMemComp).alert.alMaxI.alertaMaxIEst = ALERTA_APAGADA;

              (**ppMemComp).alert.alMinI.alertaMinIVal = MIN_I_DEFECTO;
              (**ppMemComp).alert.alMinI.alertaMinIEst = ALERTA_APAGADA;

              (**ppMemComp).alert.alMaxV.alertaMaxVVal = MAX_V_DEFECTO;
              (**ppMemComp).alert.alMaxV.alertaMaxVEst = ALERTA_APAGADA;

              (**ppMemComp).alert.alMinV.alertaMinVVal = MIN_V_DEFECTO;
              (**ppMemComp).alert.alMinV.alertaMinVEst = ALERTA_APAGADA;

              (**ppMemComp).alert.alMaxP.alertaMaxPVal = MAX_P_DEFECTO;
              (**ppMemComp).alert.alMaxP.alertaMaxPEst = ALERTA_APAGADA;

              (**ppMemComp).alert.alMinP.alertaMinPVal = MIN_P_DEFECTO;
              (**ppMemComp).alert.alMinP.alertaMinPEst = ALERTA_APAGADA;

              (**ppMemComp).alert.alMaxCon.alertaMaxConValWh = MAX_CON_WH_DEFECTO;
              (**ppMemComp).alert.alMaxCon.alertaMaxConValJ = MAX_CON_J_DEFECTO;
              (**ppMemComp).alert.alMaxCon.alertaMaxConEst = ALERTA_APAGADA;

              (**ppMemComp).alert.alMinCon.alertaMinConValWh = MIN_CON_WH_DEFECTO;
              (**ppMemComp).alert.alMinCon.alertaMinConValJ = MIN_CON_J_DEFECTO;
              (**ppMemComp).alert.alMinCon.alertaMinConEst = ALERTA_APAGADA;

              (**ppMemComp).estad.corriente_mA = I_DEFECTO;
              (**ppMemComp).estad.tensionBus_V = V_DEFECTO;
              (**ppMemComp).estad.potencia_mW = P_DEFECTO;
              (**ppMemComp).estad.energia_Wh = CON_WH_DEFECTO;
              (**ppMemComp).estad.energia_J = CON_J_DEFECTO;

              /*Inicio y configuración del semáforo*/
              //Se crea el semáforo.
              if(((*pSemId) = semget(memCompKey, CANT_SEMAFOROS,
                                     IPC_CREAT | 0666)) == ERROR_SEMGET){
                if(errno == EEXIST){
                  //Ya estaba creado, entonces se intenta abrirlo.
                  if(((*pSemId) = semget(memCompKey, CANT_SEMAFOROS,
                                         0)) == ERROR_SEMGET){
                    fprintf(stderr, "\nERROR al abrir el semaforo: %s.\n\n",
                            strerror(errno));
                    estadoPrograma = ERROR_SEMGET_ABRIR_SEM;
                  }
                  else{
                    /*Se inicializa en CANT_MAX_HIJOS el valor del semáforo.*/
                    semArg.val = CANT_MAX_HIJOS;
                    if(semctl((*pSemId), NUM_SEMAFORO, SETVAL,
                               semArg) == ERROR_SEMCTL){
                      fprintf(stderr, "\nERROR al inicializar el semaforo: "
                              "%s.\n\n", strerror(errno));
                      estadoPrograma = ERROR_SEMCTL_INICIALIZAR;
                    }
                  }
                }
                else{
                  fprintf(stderr, "\nERROR al crear el semaforo: %s.\n\n",
                          strerror(errno));
                  estadoPrograma = ERROR_SEMGET_CREAR_SEM;
                }
              }
              else{
                /*Se inicializa en CANT_MAX_HIJOS el valor del semáforo.*/
                semArg.val = CANT_MAX_HIJOS;
                if(semctl((*pSemId), NUM_SEMAFORO, SETVAL,
                           semArg) == ERROR_SEMCTL){
                  fprintf(stderr, "\nERROR al inicializar el semaforo: %s.\n\n",
                          strerror(errno));
                  estadoPrograma = ERROR_SEMCTL_INICIALIZAR;
                }
              }
            }
          }
        }
      }
    }
  }
  return estadoPrograma;
}

void TerminarServidor(int serverTCPSocket, int pipeFd[], int serialPort)
{
  /*Cuerpo de la función*/
  close(serverTCPSocket);
  close(pipeFd[0]);
  close(pipeFd[1]);
  close(serialPort);
}
/*!
*}@
*/
