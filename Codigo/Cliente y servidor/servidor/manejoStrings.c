/*!
*\addtogroup servidor
*@{
*/

/*!
*\file servidor/manejoStrings.c
*
*\date 03/02/2021
*\version 1.3
*\author Federico Alejandro Vazquez Saraullo
*
*\brief Archivo fuente que contiene las implementaciones de las funciones
*       propias para manejar strings.
*/

/*Inclusiones de bibliotecas*/
#include <stdio.h> //fgets.

#include "manejoStrings.h" //Por ser su archivo cabecera.

/*Desarrollo de funciones*/
void LeerTexto(char texto[], int tam)
{
  /*Declaraciones locales*/
  int i = 0;

  /*Cuerpo de la función*/
  //Se ingresa con fgets el string.
  fgets(texto, tam, stdin);

  //Se busca en que posición está el fin de cadena.
  while(texto[i] != '\0'){
    i++;
  }

  /*Si en la posición directamente anterior hay un salto de linea, se lo
   *reemplaza por el caracter nulo. El string entonces queda con dos
   *caracteres nulos, el primero es el que indica el fin de cadena.*/
  if(texto[i-1] == '\n'){
    texto[i-1] = '\0';
  }
}
/*!
*}@
*/
