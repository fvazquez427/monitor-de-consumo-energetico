/*!
*\addtogroup servidor
*@{
*/

/*!
*\file servidor.h
*
*\date 03/02/2021
*\version 2.0
*\author Federico Alejandro Vazquez Saraullo
*
*\brief Archivo cabecera que tiene macros y declaración de tipos de datos
*       utilizados por el main y otras funciones del servidor.
*/
#ifndef SERVIDOR_H
#define SERVIDOR_H

/*Inclusiones de bibliotecas*/
//Para poder definir la union semun.
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>

/*Macros*/
/*!
*\def   ERROR_SEND_TCP
*\brief Valor retornado por send si ocurrió algún error.
*/
#define ERROR_SEND_TCP -1

/*!
*\def   CANT_MAX_CONEXIONES_CONCURRENTES
*\brief Cantidad máxima de conexiones simultáneas al servidor.
*/
#define CANT_MAX_CONEXIONES_CONCURRENTES 10

/*!
*\def   CANT_MAX_HIJOS
*\brief Cantidad máxima de hijos.
*/
#define CANT_MAX_HIJOS (CANT_MAX_CONEXIONES_CONCURRENTES + 1)

/*!
*\def   TAM_SERIAL_PORT_RX_BUFFER
*\brief Tamaño en bytes del buffer que recibe datos del puerto serie virtual.
*/
#define TAM_SERIAL_PORT_RX_BUFFER 8

/*!
*\def   SERIAL_PORT_DELAY_USEC
*\brief Demora para que la lectura del puerto serie virtual sea correcta.
*
*\warning Está en microsegundos y debe valer al menos 100 ms para que funcione
*         bien.
*/
#define SERIAL_PORT_DELAY_USEC 100000

/*!
*\def   STDIN_FD
*\brief FD de stdin.
*/
#define STDIN_FD 0

/*!
*\def   TIMEOUT_HIJO_SEC
*\brief Parte en segundos del timeout del hijo para el select.
*
*\warning El timeout completo del hijo debe ser menor o igual al timeout total
*         del cliente.
*/
#define TIMEOUT_HIJO_SEC 1

/*!
*\def   TIMEOUT_HIJO_USEC
*\brief Parte en microsegundos del timeout del hijo para el select.
*
*\warning El timeout completo del hijo debe ser menor o igual al timeout total
*         del cliente.
*/
#define TIMEOUT_HIJO_USEC 0

/*!
*\def   TIMEOUT_MAX_HIJO_SEC
*\brief Máxima espera del hijo por pedidos de un cliente. Si no hay pedidos en
*       este tiempo, se termina la conexión (keepalive).
*
*\note Está configurado en 5 minutos.
*/
#define TIMEOUT_MAX_HIJO_SEC (60*5)

/*!
*\def   MAX_CANT_TIMEOUT_HIJO
*\brief Máxima cantidad de veces que el select del hijo sale por timeout para
*       que no se considere que no se responde al keepalive.
*/
#define MAX_CANT_TIMEOUT_HIJO (TIMEOUT_MAX_HIJO_SEC/TIMEOUT_HIJO_SEC)

/*!
*\def   STDIN_BUFFER_SIZE
*\brief Tamaño en bytes del buffer donde se ingresan strings por stdin.
*/
#define STDIN_BUFFER_SIZE 20

/*!
*\def   CONTRASENIA
*\brief Contraseña para que los clientes puedan configurar las alertas.
*/
#define CONTRASENIA "Monitor(2021)"

/*!
*\def   ERROR_CREAR_HIJO
*\brief Error al crear hijo con fork.
*/
#define ERROR_CREAR_HIJO -1

/*!
*\def   ERROR_QUITAR_HIJO
*\brief Error al quitar a un hijo del vector con QuitarHijoDelVector.
*/
#define ERROR_QUITAR_HIJO -1

/*!
*\def   ERROR_AGREGAR_HIJO
*\brief Error al agregar a un hijo al vector con AgregarHijoAlVector.
*/
#define ERROR_AGREGAR_HIJO -1

/*!
*\def   ERROR_ACEPTAR_CONEXION
*\brief Error al aceptar una conexión con accept.
*/
#define ERROR_ACEPTAR_CONEXION -1

/*!
*\def   TAM_MEMORIA_COMPARTIDA
*\brief Tamaño en bytes de la memoria compartida.
*
*\note Vale sizeof(paqueteServidor_t) ya que se almacena solo un dato del tipo
*      paqueteServidor_t.
*/
#define TAM_MEMORIA_COMPARTIDA (sizeof(paqueteServidor_t))

/*!
*\def   CANT_SEMAFOROS
*\brief Cantidad de semáforos.
*/
#define CANT_SEMAFOROS 1

/*!
*\def   ERROR_GET_TIME
*\brief Error al obtener tiempo actual con gettimeofday.
*/
#define ERROR_GET_TIME -1

/*!
*\def   DEBUGGING
*\brief Si está definida esta macro se habilitan mensajes de debugging.
*/
//#define DEBUGGING

//Tipos de datos globales.
/*!
*\enum errorPrograma_t
*\brief  Enumeración que engloba los posibles errores por los que termina el
*        programa del servidor.
*/
typedef enum{
  SIN_ERROR = 0,         /**< No hubo errores. */
  ERROR_ARGUMENTOS_MAIN, /**< Cantidad incorrecta de argumentos del main. */
  ERROR_CREAR_SOCKET,    /**< Error al crear el socket. */
  ERROR_HACER_BIND,      /**< Error al hacer bind. */
  ERROR_HACER_LISTEN,    /**< Error al hacer listen. */
  ERROR_CREAR_PIPE,      /**< Error al crear el pipe. */
  ERROR_SIGNAL_SIGCHLD, /**< Error al asocial SIGCHLD a un handler con signal.*/
  ERROR_OPEN_PUERTO_SERIE,      /**< Error al abrir el puerto serie virtual. */
  ERROR_TCGETATTR_PUERTO_SERIE, /**< Error al obtener configuración del puerto serie virtual. */
  ERROR_GET_TIME_OF_DAY, /**< Error al obtener tiempo actual con gettimeofday.*/
  ERROR_KEY_FTOK,           /**< Error al obtener key con ftok. */
  ERROR_MEM_COMP_SHMGET,    /**< Error al crear/abrir memoria compartida. */
  ERROR_MEM_COMP_SHMAT,     /**< Error al vincular memoria compartida. */
  ERROR_SEMGET_ABRIR_SEM,   /**< Error al abrir un semáforo. */
  ERROR_SEMGET_CREAR_SEM,   /**< Error al crear un semáforo. */
  ERROR_SEMCTL_INICIALIZAR, /**< Error al inicializar un semáforo con semctl. */
  ERROR_FORK,               /**< Error al crear hijo con fork. */
  ERROR_METER_HIJO_AL_VECTOR, /**< Error al agregar hijo al vector. */
  ERROR_LECTURA_CLIENTE_READ, /**< Error al leer pedido de cliente con read. */
  ERROR_LECTURA_CLIENTE_CANTIDAD_BYTES, /**< Error en cantidad de bytes leidos
                                         *del cliente con read. */
  ERROR_ESCRITURA_CLIENTE_SEND, /**< Error al enviar mensaje a un cliente con
                                 *send. */
  ERROR_ESCRITURA_PUERTO_SERIE_WRITE, /**< Error al escribir en el puerto serie
                                       *virtual con write. */
  ERROR_ESCRITURA_PUERTO_SERIE_CANTIDAD_BYTES, /**< Error en cantidad de bytes
                                                *escritos en el puerto serie
                                                *virtual con write. */
  ERROR_LECTURA_PUERTO_SERIE_READ, /**< Error al leer el puerto serie virtual
                                    *con read. */
  ERROR_LECTURA_PUERTO_SERIE_CANTIDAD_BYTES /**< Error en cantidad de bytes
                                             *leídos del puerto serie virtual
                                             *con read. */
}errorPrograma_t;

/*!
*\struct estadoVI_t
*\brief  Estructura que almacena la corriente y tensión de bus de la carga.
*/
typedef struct{
  /*!
  *\var   corriente_mA
  *\brief Corriente de la carga en miliampere.
  */
  float corriente_mA;

  /*!
  *\var   tensionBus_V
  *\brief Tensión de bus de la carga en Volt.
  *
  *\note Que sea "de bus" implica que es la medida en el pin "Vin-" del INA219.
  */
  float tensionBus_V;
}estadoVI_t;

/*!
*\struct estado_t
*\brief  Estructura que almacena la información del estado eléctrico de la
*        carga.
*/
typedef struct{
  /*!
  *\var   corriente_mA
  *\brief Corriente de la carga en miliampere.
  */
  float corriente_mA;

  /*!
  *\var   tensionBus_V
  *\brief Tensión de bus de la carga en Volt.
  *
  *\note Que sea "de bus" implica que es la medida en el pin "Vin-" del INA219.
  */
  float tensionBus_V;

  /*!
  *\var   potencia_mW
  *\brief Potencia de la carga en miliwatt.
  */
  float potencia_mW;

  /*!
  *\var   energia_Wh
  *\brief Energía consumida por la carga en Watt Hora.
  */
  float energia_Wh;

  /*!
  *\var   energia_J
  *\brief Energía consumida por la carga en Joule.
  */
  float energia_J;
}estado_t;

/*!
*\union conversionRxDatos_t
*\brief Unión que permite separar un dato estadoVI_t en cada uno de sus bytes.
*/
typedef union{
  /*!
  *\var   estado
  *\brief Estado que almacena la corriente y tensión de bus de la carga.
  */
  estadoVI_t estado;

  /*!
  *\var   estadoEnBytes
  *\brief Vector de char donde se tienen los bytes de estado.
  */
  char estadoEnBytes[TAM_SERIAL_PORT_RX_BUFFER];
}conversionRxDatos_t;

/*!
*\enum  estadoAlerta_t
*\brief Enumeración que se comporta como una alerta (dos posibles estados).
*/
typedef enum{
  ALERTA_APAGADA = 0, /**< La alerta está apagada.*/
  ALERTA_ENCENDIDA    /**< La alerta está encendida.*/
}estadoAlerta_t;

/*!
*\struct alertaMaxI_t
*\brief  Información de la alerta por máxima corriente en la carga.
*/
typedef struct{
  /*!
  *\var   alertaMaxIVal
  *\brief Valor del límite asociado a la alerta (en mA).
  */
  float alertaMaxIVal;

  /*!
  *\var   alertaMaxIEst
  *\brief Estado de la alerta (apagada o encendida).
  */
  estadoAlerta_t alertaMaxIEst;
}alertaMaxI_t;

/*!
*\struct alertaMinI_t
*\brief  Información de la alerta por mínima corriente en la carga.
*/
typedef struct{
  /*!
  *\var   alertaMinIVal
  *\brief Valor del límite asociado a la alerta (en mA).
  */
  float alertaMinIVal;

  /*!
  *\var   alertaMinIEst
  *\brief Estado de la alerta (apagada o encendida).
  */
  estadoAlerta_t alertaMinIEst;
}alertaMinI_t;

/*!
*\struct alertaMaxV_t
*\brief  Información de la alerta por máxima tensión de bus en la carga.
*/
typedef struct{
  /*!
  *\var   alertaMaxVVal
  *\brief Valor del límite asociado a la alerta (en V).
  */
  float alertaMaxVVal;

  /*!
  *\var   alertaMaxVEst
  *\brief Estado de la alerta (apagada o encendida).
  */
  estadoAlerta_t alertaMaxVEst;
}alertaMaxV_t;

/*!
*\struct alertaMinV_t
*\brief  Información de la alerta por mínima tensión de bus en la carga.
*/
typedef struct{
  /*!
  *\var   alertaMinVVal
  *\brief Valor del límite asociado a la alerta (en V).
  */
  float alertaMinVVal;

  /*!
  *\var   alertaMinVEst
  *\brief Estado de la alerta (apagada o encendida).
  */
  estadoAlerta_t alertaMinVEst;
}alertaMinV_t;

/*!
*\struct alertaMaxP_t
*\brief  Información de la alerta por máxima potencia en la carga.
*/
typedef struct{
  /*!
  *\var   alertaMaxPVal
  *\brief Valor del límite asociado a la alerta (en mW).
  */
  float alertaMaxPVal;

  /*!
  *\var   alertaMaxPEst
  *\brief Estado de la alerta (apagada o encendida).
  */
  estadoAlerta_t alertaMaxPEst;
}alertaMaxP_t;

/*!
*\struct alertaMinP_t
*\brief  Información de la alerta por mínima potencia en la carga.
*/
typedef struct{
  /*!
  *\var   alertaMinPVal
  *\brief Valor del límite asociado a la alerta (en mW).
  */
  float alertaMinPVal;

  /*!
  *\var   alertaMinPEst
  *\brief Estado de la alerta (apagada o encendida).
  */
  estadoAlerta_t alertaMinPEst;
}alertaMinP_t;

/*!
*\struct alertaMaxCon_t
*\brief  Información de la alerta por máximo consumo energético en la carga.
*/
typedef struct{
  /*!
  *\var   alertaMaxConValWh
  *\brief Valor del límite asociado a la alerta (en mW).
  */
  float alertaMaxConValWh;

  /*!
  *\var   alertaMaxConValJ
  *\brief Valor del límite asociado a la alerta (en J).
  */
  float alertaMaxConValJ;

  /*!
  *\var   alertaMaxConEst
  *\brief Estado de la alerta (apagada o encendida).
  */
  estadoAlerta_t alertaMaxConEst;
}alertaMaxCon_t;

/*!
*\struct alertaMinCon_t
*\brief  Información de la alerta por mínimo consumo energético en la carga.
*/
typedef struct{
  /*!
  *\var   alertaMinConValWh
  *\brief Valor del límite asociado a la alerta (en mW).
  */
  float alertaMinConValWh;

  /*!
  *\var   alertaMinConValJ
  *\brief Valor del límite asociado a la alerta (en J).
  */
  float alertaMinConValJ;

  /*!
  *\var   alertaMinConEst
  *\brief Estado de la alerta (apagada o encendida).
  */
  estadoAlerta_t alertaMinConEst;
}alertaMinCon_t;

/*!
*\struct alertas_t
*\brief  Información del valor y estado de todas las alertas de la carga.
*/
typedef struct{
  /*!
  *\var   alMaxI
  *\brief Información de la alerta por máxima corriente en la carga.
  */
  alertaMaxI_t   alMaxI;

  /*!
  *\var   alMinI
  *\brief Información de la alerta por mínima corriente en la carga.
  */
  alertaMinI_t   alMinI;

  /*!
  *\var   alMaxV
  *\brief Información de la alerta por máxima tensión de bus en la carga.
  */
  alertaMaxV_t   alMaxV;

  /*!
  *\var   alMinV
  *\brief Información de la alerta por mínima tensión de bus en la carga.
  */
  alertaMinV_t   alMinV;

  /*!
  *\var   alMaxP
  *\brief Información de la alerta por máxima potencia en la carga.
  */
  alertaMaxP_t   alMaxP;

  /*!
  *\var   alMinP
  *\brief Información de la alerta por mínima potencia en la carga.
  */
  alertaMinP_t   alMinP;

  /*!
  *\var   alMaxCon
  *\brief Información de la alerta por máximo consumo energético en la carga.
  */
  alertaMaxCon_t alMaxCon;

  /*!
  *\var   alMinCon
  *\brief Información de la alerta por mínimo consumo energético en la carga.
  */
  alertaMinCon_t alMinCon;
}alertas_t;

/*!
*\struct paqueteServidor_t
*\brief  Paquete de información enviado por el servidor, el cual contiene
*        el estado eléctrico actual de la carga y el estado actual de todas sus
*        alertas.
*/
typedef struct{
  /*!
  *\var   estad
  *\brief Estado eléctrico actual de la carga.
  */
  estado_t estad;

  /*!
  *\var   alert
  *\brief Estado actual de todas las alertas de la carga.
  */
  alertas_t alert;
}paqueteServidor_t;

/*!
*\enum  pedidoEstado_t
*\brief Pedido afirmativo o negativo del estado eléctrico actual de la carga.
*/
typedef enum{
  PEDIR_ESTADO_NO = 0, /**< SI se pide el estado eléctrico actual.*/
  PEDIR_ESTADO_SI      /**< NO se pide el estado eléctrico actual.*/
}pedidoEstado_t;

/*!
*\enum  pedidoAlertas_t
*\brief Pedido afirmativo o negativo de configuración de las alertas.
*/
typedef enum{
  CAMBIAR_ALERTAS_NO = 0, /**< SI se pide configurar las alertas.*/
  CAMBIAR_ALERTAS_SI      /**< NO se pide configurar las alertas.*/
}pedidoAlertas_t;

/*!
*\struct pedidoDelCliente_t
*\brief  Paquete enviado por el cliente al servidor para iniciar un pedido, sea
*        del estado eléctrico actual de la carga y/o de configuración de las
*        alertas.
*/
typedef struct{
  /*!
  *\var   pedEstado
  *\brief Pedido afirmativo o negativo del estado eléctrico actual de la carga.
  */
  pedidoEstado_t pedEstado;

  /*!
  *\var   pedAlertas
  *\brief Pedido afirmativo o negativo de la configuración de las alertas.
  */
  pedidoAlertas_t pedAlertas;
}pedidoDelCliente_t;

/*!
*\enum  bandera_t
*\brief Enumeración que se comporta como una bandera (dos posibles estados).
*/
typedef enum{
  BANDERA_BAJA = 0, /**< La bandera está baja.*/
  BANDERA_ALTA      /**< La bandera está alta o izada.*/
}bandera_t;

/*!
*\enum  mensajeDelServidor_t
*\brief Pedido de información que hace el servidor en la comunicación con el
*       cliente.
*/
typedef enum{
  SERVIDOR_PIDE_CONTRASENIA = 0,         /**< Se pide la contraseña para
                                         configurar las alertas.*/
  SERVIDOR_PIDE_CONFIGURACION,           /**< Se pide la información de
                                         configuración de las alertas.*/
  SERVIDOR_AVISA_CONTRASENIA_INCORRECTA, /**< El servidor avisa que la
                                         contraseña ingresada es incorrecta.*/
  SERVIDOR_AVISA_CONFIGURACION_HECHA     /**< El servidor avisa que la
                                         configuración de las alertas se hizo
                                         correctamente.*/
}mensajeDelServidor_t;

/*!
*\union semun
*\brief Unión para semáforos.
*/
union semun
{
  /*!
  *\var   val
  *\brief Valor para SETVAL.
  */
  int val;

  /*!
  *\var   buf
  *\brief Buffer para IPC_STAT e IPC_SET.
  */
  struct semid_ds *buf;

  /*!
  *\var   array
  *\brief Vector para GETALL y SETALL.
  */
  unsigned short int *array;

  /*!
  *\var   __buf
  *\brief Buffer para IPC_INFO (específico de Linux).
  */
  struct seminfo *__buf;
};
#endif //SERVIDOR_H
/*!
*}@
*/
