/*!
*\addtogroup servidor
*@{
*/

/*!
*\file servidor/manejoStrings.h
*
*\date 03/02/2021
*\version 1.3
*\author Federico Alejandro Vazquez Saraullo
*
*\brief Archivo cabecera que contiene los prototipos de funciones propias para
*       manejar strings.
*/

#ifndef MANEJO_STRINGS_H
#define MANEJO_STRINGS_H

/*Prototipos de funciones*/
/*!
*\fn void LeerTexto(char texto[], int tam)
*
*\brief Función que permite ingresar un string "texto" de tamaño "tam" por
*       medio del teclado de forma segura y óptima. En caso de quedar un salto
*       de línea, se lo cambia por el fin de cadena.
*
*\param[in,out] texto (char*) Vector de char donde se guarda el string.
*\param[in]     tam   (int)   Tamaño del vector de char "texto".
*
*\return void.
*/
void LeerTexto(char[], int);

#endif //MANEJO_STRINGS_H
/*!
*}@
*/
