/*!
*\addtogroup servidor
*@{
*/

/*!
*\file calculos.c
*
*\date 03/02/2021
*\version 2.0
*\author Federico Alejandro Vazquez Saraullo
*
*\brief Archivo fuente que contiene las implementaciones de las funciones
*       para hacer cálculos, actualizar la memoria compartida y el estado de las
*       alertas, y enviar el estado actual al cliente.
*/

/*Inclusiones de bibliotecas*/
#include <stdio.h>      //fprintf.
#include <sys/time.h>   //struct timeval y gettimeofday.
#include <sys/types.h>  //Semáforos y send.
#include <sys/socket.h> //send.
#include <sys/ipc.h>    //Semáforos.
#include <sys/sem.h>    //Semáforos.
#include <stddef.h>     //NULL.
#include <string.h>     //strerror.
#include <errno.h>      //errno.

#include "calculos.h" //Por ser su archivo cabecera.

/*Desarrollo de funciones*/
void CalcularPotenciaYEnergia(
  estado_t *pEstadoActual,
  struct timeval *pTiempoUltimaLectura)
{
  /*Declaraciones locales*/
  struct timeval tiempoActual;
  struct timeval difTiempo;
  unsigned long int difTiempo_us;
  float difTiempo_s;

  /*Cuerpo de la función*/
  //Se calcula la potencia.
  (pEstadoActual->potencia_mW) = (pEstadoActual->tensionBus_V) * \
                                 (pEstadoActual->corriente_mA);

  //Se calcula la energía. Primero se obtiene el tiempo transcurrido en us.
  gettimeofday(&tiempoActual, NULL);

  difTiempo.tv_sec = tiempoActual.tv_sec - (pTiempoUltimaLectura->tv_sec);
  difTiempo.tv_usec = tiempoActual.tv_usec - (pTiempoUltimaLectura->tv_usec);
  //Se revierte el carry en caso de ser necesario para los us.
  if(difTiempo.tv_usec < 0){
    difTiempo.tv_sec --;
    difTiempo.tv_usec += 1000000;
  }

  difTiempo_us = (unsigned long int) difTiempo.tv_usec + \
                 (unsigned long int) (difTiempo.tv_sec * 1000000);
  difTiempo_s = (float) difTiempo_us / 1000000;

  //Se calcula finalmente la energia.
  (pEstadoActual->energia_Wh) += ((pEstadoActual->potencia_mW) / 1000) * \
                                 (difTiempo_s / 3600);

  (pEstadoActual->energia_J) += ((pEstadoActual->potencia_mW) / 1000) * \
                                difTiempo_s;

  //Se actualiza el valor de tiempoUltimaLectura.
  (*pTiempoUltimaLectura) = tiempoActual;
}

void ActualizarAlertas(
  alertas_t alertasConfiguradas,
  int semId,
  paqueteServidor_t *pMemComp)
{
  /*Declaraciones locales*/
  struct sembuf semBuf;

  /*Cuerpo de la función*/
  /*Se intenta tomar el semáforo para escribir de forma segura en la memoria
   *compartida. Si al menos un hijo la sigue leyendo, se espera a que este
   *termine porque se bloquea.*/
  semBuf.sem_num = 0;
  semBuf.sem_op = DEC_HIJO_ESCRIBE_MEMORIA;
  semBuf.sem_flg = 0;
  semop(semId, &semBuf, CANT_SEMAFOROS);

  //Se graban solo los valores de las alertas en la memoria compartida.
  (*pMemComp).alert.alMaxI.alertaMaxIVal = alertasConfiguradas.alMaxI.alertaMaxIVal;
  (*pMemComp).alert.alMinI.alertaMinIVal = alertasConfiguradas.alMinI.alertaMinIVal;
  (*pMemComp).alert.alMaxV.alertaMaxVVal = alertasConfiguradas.alMaxV.alertaMaxVVal;
  (*pMemComp).alert.alMinV.alertaMinVVal = alertasConfiguradas.alMinV.alertaMinVVal;
  (*pMemComp).alert.alMaxP.alertaMaxPVal = alertasConfiguradas.alMaxP.alertaMaxPVal;
  (*pMemComp).alert.alMinP.alertaMinPVal = alertasConfiguradas.alMinP.alertaMinPVal;
  (*pMemComp).alert.alMaxCon.alertaMaxConValJ = alertasConfiguradas.alMaxCon.alertaMaxConValJ;
  (*pMemComp).alert.alMaxCon.alertaMaxConValWh = alertasConfiguradas.alMaxCon.alertaMaxConValWh;
  (*pMemComp).alert.alMinCon.alertaMinConValJ = alertasConfiguradas.alMinCon.alertaMinConValJ;
  (*pMemComp).alert.alMinCon.alertaMinConValWh = alertasConfiguradas.alMinCon.alertaMinConValWh;

  //Se libera el semáforo.
  semBuf.sem_op = INC_HIJO_ESCRIBE_MEMORIA;
  semop(semId, &semBuf, CANT_SEMAFOROS);
}

void AnalizarAlertas(
  paqueteServidor_t *pPaqSer,
  int semId,
  paqueteServidor_t* pMemComp)
{
  /*Declaraciones locales*/
  struct sembuf semBuf;
  alertas_t alertasActualizadas;

  /*Cuerpo de la función*/
  /*Se intenta tomar el semáforo para leer de forma segura la memoria
   *compartida. Si un hijo la sigue escribiendo, se espera a que este termine
   *porque se bloquea.*/
  semBuf.sem_num = 0;
  semBuf.sem_op = DEC_HIJO_LEE_MEMORIA;
  semBuf.sem_flg = 0;
  semop(semId, &semBuf, CANT_SEMAFOROS);

  //Se lee de la memoria compartida y rápidamente se libera el semáforo.
  alertasActualizadas = (*pMemComp).alert;
  semBuf.sem_op = INC_HIJO_LEE_MEMORIA;
  semop(semId, &semBuf, CANT_SEMAFOROS);

  //Se actualizan los valores y estados de las alertas en *pPaqSer.
  (*pPaqSer).alert.alMaxI.alertaMaxIVal = alertasActualizadas.alMaxI.alertaMaxIVal;
  (*pPaqSer).alert.alMinI.alertaMinIVal = alertasActualizadas.alMinI.alertaMinIVal;
  (*pPaqSer).alert.alMaxV.alertaMaxVVal = alertasActualizadas.alMaxV.alertaMaxVVal;
  (*pPaqSer).alert.alMinV.alertaMinVVal = alertasActualizadas.alMinV.alertaMinVVal;
  (*pPaqSer).alert.alMaxP.alertaMaxPVal = alertasActualizadas.alMaxP.alertaMaxPVal;
  (*pPaqSer).alert.alMinP.alertaMinPVal = alertasActualizadas.alMinP.alertaMinPVal;
  (*pPaqSer).alert.alMaxCon.alertaMaxConValJ = alertasActualizadas.alMaxCon.alertaMaxConValJ;
  (*pPaqSer).alert.alMaxCon.alertaMaxConValWh = alertasActualizadas.alMaxCon.alertaMaxConValWh;
  (*pPaqSer).alert.alMinCon.alertaMinConValJ = alertasActualizadas.alMinCon.alertaMinConValJ;
  (*pPaqSer).alert.alMinCon.alertaMinConValWh = alertasActualizadas.alMinCon.alertaMinConValWh;

  //Alerta por máxima corriente.
  if((*pPaqSer).estad.corriente_mA >= (*pPaqSer).alert.alMaxI.alertaMaxIVal){
    (*pPaqSer).alert.alMaxI.alertaMaxIEst = ALERTA_ENCENDIDA;
  }
  else{
    (*pPaqSer).alert.alMaxI.alertaMaxIEst = ALERTA_APAGADA;
  }

  //Alerta por mínima corriente.
  if((*pPaqSer).estad.corriente_mA <= (*pPaqSer).alert.alMinI.alertaMinIVal){
    (*pPaqSer).alert.alMinI.alertaMinIEst = ALERTA_ENCENDIDA;
  }
  else{
    (*pPaqSer).alert.alMinI.alertaMinIEst = ALERTA_APAGADA;
  }

  //Alerta por máxima tensión.
  if((*pPaqSer).estad.tensionBus_V >= (*pPaqSer).alert.alMaxV.alertaMaxVVal){
    (*pPaqSer).alert.alMaxV.alertaMaxVEst = ALERTA_ENCENDIDA;
  }
  else{
    (*pPaqSer).alert.alMaxV.alertaMaxVEst = ALERTA_APAGADA;
  }

  //Alerta por mínima tensión.
  if((*pPaqSer).estad.tensionBus_V <= (*pPaqSer).alert.alMinV.alertaMinVVal){
    (*pPaqSer).alert.alMinV.alertaMinVEst = ALERTA_ENCENDIDA;
  }
  else{
    (*pPaqSer).alert.alMinV.alertaMinVEst = ALERTA_APAGADA;
  }

  //Alerta por máxima potencia.
  if((*pPaqSer).estad.potencia_mW >= (*pPaqSer).alert.alMaxP.alertaMaxPVal){
    (*pPaqSer).alert.alMaxP.alertaMaxPEst = ALERTA_ENCENDIDA;
  }
  else{
    (*pPaqSer).alert.alMaxP.alertaMaxPEst = ALERTA_APAGADA;
  }

  //Alerta por mínima potencia.
  if((*pPaqSer).estad.potencia_mW <= (*pPaqSer).alert.alMinP.alertaMinPVal){
    (*pPaqSer).alert.alMinP.alertaMinPEst = ALERTA_ENCENDIDA;
  }
  else{
    (*pPaqSer).alert.alMinP.alertaMinPEst = ALERTA_APAGADA;
  }

  /*Alerta por máximo consumo energético. Se pone como condición que se alcance
   *el límite por Wh o J debido a que como se calculan puede ser que uno llegue
   *al límite antes que otro por el error de cálculo cometido .*/
  if( ((*pPaqSer).estad.energia_Wh >= (*pPaqSer).alert.alMaxCon.alertaMaxConValWh) || \
      ((*pPaqSer).estad.energia_J >= (*pPaqSer).alert.alMaxCon.alertaMaxConValJ) ){
    (*pPaqSer).alert.alMaxCon.alertaMaxConEst = ALERTA_ENCENDIDA;
  }
  else{
    (*pPaqSer).alert.alMaxCon.alertaMaxConEst = ALERTA_APAGADA;
  }

  /*Alerta por mínimo consumo energético. Se pone como condición que se alcance
   *el límite por Wh o J debido a que como se calculan puede ser que uno llegue
   *al límite antes que otro por el error de cálculo cometido .*/
  if( ((*pPaqSer).estad.energia_Wh <= (*pPaqSer).alert.alMinCon.alertaMinConValWh) || \
      ((*pPaqSer).estad.energia_J <= (*pPaqSer).alert.alMinCon.alertaMinConValJ) ){
    (*pPaqSer).alert.alMinCon.alertaMinConEst = ALERTA_ENCENDIDA;
  }
  else{
    (*pPaqSer).alert.alMinCon.alertaMinConEst = ALERTA_APAGADA;
  }
}

void ActualizarMemoria(
  paqueteServidor_t paqSer,
  int semId,
  paqueteServidor_t* pMemComp)
{
  /*Declaraciones locales*/
  struct sembuf semBuf;

  /*Cuerpo de la función*/
  /*Se intenta tomar el semáforo para escribir de forma segura la memoria
   *compartida.*/
  semBuf.sem_num = 0;
  semBuf.sem_op = DEC_HIJO_ESCRIBE_MEMORIA;
  semBuf.sem_flg = 0;
  semop(semId, &semBuf, CANT_SEMAFOROS);

  //Se actualiza la memoria compartida.
  (*pMemComp).estad = paqSer.estad;

  (*pMemComp).alert.alMaxI.alertaMaxIEst = paqSer.alert.alMaxI.alertaMaxIEst;
  (*pMemComp).alert.alMinI.alertaMinIEst = paqSer.alert.alMinI.alertaMinIEst;
  (*pMemComp).alert.alMaxV.alertaMaxVEst = paqSer.alert.alMaxV.alertaMaxVEst;
  (*pMemComp).alert.alMinV.alertaMinVEst = paqSer.alert.alMinV.alertaMinVEst;
  (*pMemComp).alert.alMaxP.alertaMaxPEst = paqSer.alert.alMaxP.alertaMaxPEst;
  (*pMemComp).alert.alMinP.alertaMinPEst = paqSer.alert.alMinP.alertaMinPEst;
  (*pMemComp).alert.alMaxCon.alertaMaxConEst = paqSer.alert.alMaxCon.alertaMaxConEst;
  (*pMemComp).alert.alMinCon.alertaMinConEst = paqSer.alert.alMinCon.alertaMinConEst;

  //Se libera el semáforo.
  semBuf.sem_op = INC_HIJO_ESCRIBE_MEMORIA;
  semop(semId, &semBuf, CANT_SEMAFOROS);
}

void EnviarEstadoAlCliente(
  int newClientTCPSocket,
  int semId,
  paqueteServidor_t* pMemComp,
  errorPrograma_t* pEstProg)
{
  /*Declaraciones locales*/
  struct sembuf semBuf;
  paqueteServidor_t paqSer;

  /*Cuerpo de la función*/
  /*Se intenta tomar el semáforo para leer de forma segura la memoria
   *compartida. Si un hijo la sigue escribiendo, se espera a que este termine
   *porque se bloquea.*/
  semBuf.sem_num = 0;
  semBuf.sem_op = DEC_HIJO_LEE_MEMORIA;
  semBuf.sem_flg = 0;
  semop(semId, &semBuf, CANT_SEMAFOROS);

  //Se lee de la memoria compartida y rápidamente se libera el semáforo.
  paqSer = (*pMemComp);
  semBuf.sem_op = INC_HIJO_LEE_MEMORIA;
  semop(semId, &semBuf, CANT_SEMAFOROS);

  /*Se envía el estado por TCP al cliente.*/
  if(send(newClientTCPSocket, &paqSer, sizeof(paqSer), 0) == ERROR_SEND_TCP){
    fprintf(stderr, "\nERROR al enviar el estado actual al cliente por TCP: "
            "%s.\n", strerror(errno));
    (*pEstProg) = ERROR_ESCRITURA_CLIENTE_SEND;
  }
}
/*!
*}@
*/
