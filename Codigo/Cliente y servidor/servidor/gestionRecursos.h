/*!
*\addtogroup servidor
*@{
*/

/*!
*\file gestionRecursos.h
*
*\date 03/02/2021
*\version 2.0
*\author Federico Alejandro Vazquez Saraullo
*
*\brief Archivo cabecera que contiene macros y prototipos de funciones propias
*       para gestionar los recursos del servidor. En cuanto a las
*       características, las funciones corresponden a la inicialización y
*       finalización del servidor.
*/
#ifndef GESTION_RECURSOS_H
#define GESTION_RECURSOS_H

/*Inclusiones de bibliotecas*/
#include "servidor.h"     /*errorPrograma_t, paqueteServidor_t y para
                           *gestionRecursos.c.*/
#include "gestionHijos.h" //hijo_t y para gestionRecursos.c.

/*Macros*/
/*!
*\def   CANT_MAIN_ARG
*\brief Cantidad de argumento del main.
*/
#define CANT_MAIN_ARG 2

/*!
*\def   SOCKET_ERROR
*\brief Valor retornado por socket si ocurrió algún error.
*/
#define SOCKET_ERROR -1

/*!
*\def   BIND_ERROR
*\brief Valor retornado por bind si ocurrió algún error.
*/
#define BIND_ERROR -1

/*!
*\def   LISTEN_ERROR
*\brief Valor retornado por listen si ocurrió algún error.
*/
#define LISTEN_ERROR -1

/*!
*\def   SERVIDOR_PUERTO_TCP
*\brief Puerto TCP del servidor.
*/
#define SERVIDOR_PUERTO_TCP 10000

/*!
*\def   CANT_MAX_CONEXIONES_ENCOLADAS
*\brief Tamaño de la cola de pedidos pendientes de conexión.
*
*\warning Este valor es obligatorio ponerlo al ejecutar la función "listen",
*         aunque no tiene mucho efecto en verdad porque la cola es mucho más
*         grande.
*/
#define CANT_MAX_CONEXIONES_ENCOLADAS 5

/*!
*\def   PIPE_ERROR
*\brief Valor retornado por pipe si ocurrió algún error.
*/
#define PIPE_ERROR -1

/*!
*\def   SERIAL_PORT_OK
*\brief Valor retornado por tcgetattr y tcsetattr si no ocurrió algún error.
*/
#define SERIAL_PORT_OK 0

/*!
*\def   SERIAL_PORT_ERROR
*\brief Valor retornado por open si ocurrió algún error al abrir el puerto serie
*       virtual.
*/
#define SERIAL_PORT_ERROR -1

/*!
*\def   TIMEOUT_SERIAL_PORT_DSEC
*\brief Demora máxima para la lectura del puerto serie virtual.
*
*\warning Está en decisegundos (ds).
*/
#define TIMEOUT_SERIAL_PORT_DSEC 10

/*!
*\def   SERIAL_PORT_DELAY_SEC
*\brief Demora necesaria luego de abrir y configurar el puerto serie virtual
*       para que la inicialización de este quede efectivamente hecha.
*
*\note    Su valor es el mínimo obtenido empíricamente.
*\warning Está en segundos (s).
*/
#define SERIAL_PORT_DELAY_SEC 2

/*!
*\def   ERROR_FTOK
*\brief Valor retornado por ftok si ocurrió algún error.
*/
#define ERROR_FTOK -1

/*!
*\def   ERROR_SHMGET
*\brief Valor retornado por shmget si ocurrió algún error.
*/
#define ERROR_SHMGET -1

/*!
*\def   ERROR_SHMAT
*\brief Valor retornado por shmat si ocurrió algún error.
*/
#define ERROR_SHMAT -1

/*!
*\def   ERROR_SEMGET
*\brief Valor retornado por semget si ocurrió algún error.
*/
#define ERROR_SEMGET -1

/*!
*\def   ERROR_SEMCTL
*\brief Valor retornado por semctl si ocurrió algún error.
*/
#define ERROR_SEMCTL -1

/*!
*\def   I_DEFECTO
*\brief Valor de corriente (en mA) que tiene el estado eléctrico inicial (por
*       defecto).
*/
#define I_DEFECTO 0

/*!
*\def   V_DEFECTO
*\brief Valor de tensión de bus (en V) que tiene el estado eléctrico inicial
*       (por defecto).
*/
#define V_DEFECTO 0

/*!
*\def   P_DEFECTO
*\brief Valor de potencia (en mW) que tiene el estado eléctrico inicial (por
*       defecto).
*/
#define P_DEFECTO 0

/*!
*\def   CON_J_DEFECTO
*\brief Valor de energía (en J) que tiene el estado eléctrico inicial (por
*       defecto).
*/
#define CON_J_DEFECTO 0

/*!
*\def   CON_WH_DEFECTO
*\brief Valor de energía (en Wh) que tiene el estado eléctrico inicial (por
*       defecto).
*/
#define CON_WH_DEFECTO 0

/*!
*\def   MAX_I_DEFECTO
*\brief Valor inicial de corriente (en mA) que tiene la alerta por máxima
*       corriente.
*/
#define MAX_I_DEFECTO 10.0

/*!
*\def   MIN_I_DEFECTO
*\brief Valor inicial de corriente (en mA) que tiene la alerta por mínima
*       corriente.
*/
#define MIN_I_DEFECTO 1.0

/*!
*\def   MAX_V_DEFECTO
*\brief Valor inicial de tensión de bus (en V) que tiene la alerta por máxima
*       tensión.
*/
#define MAX_V_DEFECTO 10.0

/*!
*\def   MIN_V_DEFECTO
*\brief Valor inicial de tensión de bus (en V) que tiene la alerta por mínima
*       tensión.
*/
#define MIN_V_DEFECTO 1.0

/*!
*\def   MAX_P_DEFECTO
*\brief Valor inicial de potencia (en mW) que tiene la alerta por máxima
*       potencia.
*/
#define MAX_P_DEFECTO 40.0

/*!
*\def   MIN_P_DEFECTO
*\brief Valor inicial de potencia (en mW) que tiene la alerta por mínima
*       potencia.
*/
#define MIN_P_DEFECTO 1.0

/*!
*\def   MAX_CON_WH_DEFECTO
*\brief Valor inicial de energía (en Wh) que tiene la alerta por máxima
*       energía.
*
*\warning Debe ser equivalente en Joules a MAX_CON_J_DEFECTO.
*/
#define MAX_CON_WH_DEFECTO 1

/*!
*\def   MAX_CON_J_DEFECTO
*\brief Valor inicial de energía (en J) que tiene la alerta por máxima
*       energía.
*
*\warning Debe ser equivalente en Watt Hora a MAX_CON_WH_DEFECTO.
*/
#define MAX_CON_J_DEFECTO 3600

/*!
*\def   MIN_CON_WH_DEFECTO
*\brief Valor inicial de energía (en Wh) que tiene la alerta por mínima
*       energía.
*
*\warning Debe ser equivalente en Joules a MIN_CON_J_DEFECTO.
*/
#define MIN_CON_WH_DEFECTO -1

/*!
*\def   MIN_CON_J_DEFECTO
*\brief Valor inicial de energía (en J) que tiene la alerta por mínima
*       energía.
*
*\warning Debe ser equivalente en Watt Hora a MIN_CON_WH_DEFECTO.
*/
#define MIN_CON_J_DEFECTO -3600

/*!
*\def   NUM_SEMAFORO
*\brief Número de identificación del semáforo utilizado.
*/
#define NUM_SEMAFORO 0
/*Prototipos de funciones*/

/*!
*\fn errorPrograma_t IniciarServidor(
*      int argc,
*      char *argv[],
*      int *pServerTCPSocket,
*      int pipeFd[],
*      paqueteServidor_t **ppMemComp,
*      int *pSemId,
*      int *pSerialPort,
*      hijo_t vecHijos[],
*      int cantHijos)
*
*\brief Función que permite inicializar al servidor, iniciando todos los
*       recursos necesarios para ello.
*
*\details Función que permite verificar la cantidad de argumentos por el main;
*         crea el socket, lo asocia con la IP Local y el puerto
*         SERVIDOR_PUERTO_TCP y lo pone a escuchar; crea el pipe, la memoria
*         compartida y el semáforo, inicializando esta memoria; asocia la señal
*         SIGCHL con Handler_ReconocerHijoMuerto; abre el puerto serie virtual y
*         lo configura; inicializa el vector de hijos.
*
*\param[in]      argc   (int)    Cantidad de argumentos recibidos por el main.
*\param[in, out] argv   (char**) Vector de strings de los argumentos del main.
*\param[in, out] pServerTCPSocket (int*) Puntero al socket TCP del servidor.
*\param[in, out] pipeFd (int*) Vector de los FD del pipe creado.
*\param[in, out] ppMemComp (paqueteServidor_t**) Puntero al puntero que apunta
*                 al inicio de la memoria compartida.
*\param[in, out] pSemId      (int*)    Puntero al ID del semáforo.
*\param[in, out] pSerialPort (int*)    Puntero al FD del puerto serie virtual.
*\param[in, out] vecHijos    (hijo_t*) Vector de hijos.
*\param[in]      cantHijos   (int)     Cantidad de hijos máxima posible.
*
*\return errorPrograma_t con el código del error que sucedió (si es que hubo
*        alguno).
*/

errorPrograma_t IniciarServidor(int, char **, int *, int [],
                                paqueteServidor_t **, int *, int *, hijo_t [],
                                int);

/*!
*\fn void TerminarServidor(int serverTCPSocket, int pipeFd[], int serialPort)
*
*\brief Función que finaliza recursos del servidor.
*
*\details Se cierra el socket principal TCP del servidor, el pipe y el puerto
*         serie virtual.
*/
void TerminarServidor(int, int[], int);

#endif //GESTION_RECURSOS_H
/*!
*}@
*/
