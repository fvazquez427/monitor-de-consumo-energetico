/*!
*\addtogroup servidor
*@{
*/

/*!
*\file gestionHijos.h
*
*\date 03/02/2021
*\version 2.0
*\author Federico Alejandro Vazquez Saraullo
*
*\brief Archivo cabecera que contiene macros, tipos de datos y prototipos de
*       funciones propias para gestionar la base de datos (vector) de hijos.
*/

#ifndef GESTION_HIJOS_H
#define GESTION_HIJOS_H

/*Macros*/
/*!
*\def   KILL_ERROR
*\brief Valor retornado por kill si ocurrió algún error.
*/
#define KILL_ERROR -1

/*!
*\def   WAIT_ERROR
*\brief Valor retornado por wait si ocurrió algún error.
*/
#define WAIT_ERROR -1

//Tipos de datos globales.
/*!
*\enum  EstadoPIDHijo_t
*\brief Enumeración que indica el estado de cierto PID en la base de datos de
*       hijos. Puede estar disponible u ocupado.
*/
typedef enum{
  PID_HIJO_DISPONIBLE = 0, /**< El PID está disponible.*/
  PID_HIJO_OCUPADO         /**< El PID está ocupado.*/
}EstadoPIDHijo_t;

/*!
*\struct  hijo_t
*\brief Estructura que almacena la información de un hijo.
*/
typedef struct{
  /*!
  *\var   hijoPID
  *\brief PID del hijo.
  */
  pid_t hijoPID;

  /*!
  *\var   estHijo
  *\brief Estado del PID del hijo.
  */
  EstadoPIDHijo_t estHijo;
}hijo_t;

/*Prototipos de funciones*/
/*!
*\fn void Handler_ReconocerHijoMuerto(int val)
*
*\brief Handler que se ejecuta cuando un hijo termina su ejecución para
*       reconocer la muerte de este y, en caso que la macro DEBUGGING (de
*       servidor.h) esté definida, mostrar información de depuración.
*
*\param[in] val (int) Valor que retorna el hijo al terminar su ejecución.
*\return void.
*/
void Handler_ReconocerHijoMuerto(int);

/*!
*\fn void InicializarVectorDeHijos(hijo_t vecHijos[], int cantHijos)
*
*\brief Función que inicializa el vector de hijos "vecHijos" de "cantHijos"
*       elementos con todos sus lugares disponibles.
*
*\param[in, out] vecHijos  (hijo_t*) Vector de hijos.
*\param[in]      cantHijos (int)     Cantidad de hijos en el vector.
*
*\return void.
*/
void InicializarVectorDeHijos(hijo_t[], int);

/*!
*\fn int AgregarHijoAlVector(
*      hijo_t vecHijos[],
*      int cantHijos,
*      pid_t PIDHijoNuevo)
*
*\brief Función que agrega al "PIDHijoNuevo" en el primer lugar disponible
*       que encuentre en el vector de hijos "vecHijos", el cual tiene
*       "cantHijos" elementos.

*\param[in, out] vecHijos     (hijo_t*) Vector de hijos.
*\param[in]      cantHijos    (int)     Cantidad de hijos en el vector.
*\param[in]      PIDHijoNuevo (pid_t)   PID del hijo a agregar al vector.
*
*\return int que vale 0 si salió todo bien o -1 si están todos los lugares
*        ocupados.
*/
int AgregarHijoAlVector(hijo_t[], int, pid_t);

/* int QuitarHijoDelVector(
*    hijo_t vecHijos[],
*    int cantHijos,
*    pid_t PIDHijoAQuitar)
*
*\brief Función que saca al "PIDHijoAQuitar" del vector "vecHijos", es decir,
*       pone su lugar como disponible. Dicho vector tiene "cantHijos" elementos.
*
*\param[in, out] vecHijos       (hijo_t*) Vector de hijos.
*\param[in]      cantHijos      (int)     Cantidad de hijos en el vector.
*\param[in]      PIDHijoAQuitar (pid_t)   PID del hijo a quitar del vector.
*
*\return int que vale 0 si salió todo bien o -1 si no se lo pudo sacar porque
*        no estaba en el vector.
*/
int QuitarHijoDelVector(hijo_t[], int, pid_t);

/*!
*\fn int TerminarYSacarTodosLosHijos(hijo_t vecHijos[], int cantHijos)
*
*\brief Función que termina la ejecución de todos los hijos vivos enviándoles la
*       señal SIGKILL, para luego sacarlos del vector "vecHijos". Este vector
*       tiene "cantHijos" elementos.
*
*\param[in, out] vecHijos       (hijo_t*) Vector de hijos.
*\param[in]      cantHijos      (int)     Cantidad de hijos en el vector.
*
*\return int que vale 0 si salió todo bien o -1 si hubo algún error al enviar
*        SIGKILL.
*/
int TerminarYSacarTodosLosHijos(hijo_t[], int);

#endif //GESTION_HIJOS_H
/*!
*}@
*/
