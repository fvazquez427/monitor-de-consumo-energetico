/*!
*\addtogroup servidor
*@{
*/

/*!
*\file calculos.h
*
*\date 03/02/2021
*\version 2.0
*\author Federico Alejandro Vazquez Saraullo
*
*\brief Archivo cabecera que contiene macros y prototipos de funciones propias
*       para hacer cálculos, actualizar la memoria compartida y el estado de las
*       alertas, y enviar el estado actual al cliente.
*/
#ifndef CALCULOS_H
#define CALCULOS_H

/*Inclusiones de bibliotecas*/
#include <sys/time.h> //struct timeval y gettimeofday.

#include "servidor.h" /*estado_t, paqueteServidor_t, alertas_t, errorPrograma_t
                       *y para calculos.c.*/

/*Macros*/
/*!
*\def   DEC_HIJO_ESCRIBE_MEMORIA
*\brief Valor en que se decrece al semáforo cuando un hijo escribe en la memoria
*       compartida.
*/
#define DEC_HIJO_ESCRIBE_MEMORIA (-CANT_MAX_HIJOS)

/*!
*\def   INC_HIJO_ESCRIBE_MEMORIA
*\brief Valor en que se incrementa al semáforo cuando un hijo escribe en la
*       memoria compartida.
*/
#define INC_HIJO_ESCRIBE_MEMORIA (CANT_MAX_HIJOS)

/*!
*\def   DEC_HIJO_LEE_MEMORIA
*\brief Valor en que se decrece al semáforo cuando un hijo lee la memoria
*       compartida.
*/
#define DEC_HIJO_LEE_MEMORIA -1

/*!
*\def   INC_HIJO_LEE_MEMORIA
*\brief Valor en que se incrementa al semáforo cuando un hijo lee la memoria
*       compartida.
*/
#define INC_HIJO_LEE_MEMORIA 1

/*Prototipos de funciones*/

/*!
*\fn void CalcularPotenciaYEnergia(
*      estado_t *pEstadoActual,
*      struct timeval *pTiempoUltimaLectura)
*
*\brief Función que permite calcular la potencia en miliwatt y la energía en
*       Joule y Wh según el estado actual y el tiempo de la última lectura.
*
*\param[in, out] pEstadoActual        (estado_t *)     Puntero al estado actual.
*\param[in, out] pTiempoUltimaLectura (struct timeval *) Puntero al tiempo de la
*                                                        última lectura.
*
*\return void.
*/
void CalcularPotenciaYEnergia(estado_t *, struct timeval *);

/*!
*\fn void ActualizarAlertas(
*      alertas_t alertasConfiguradas,
*      int semId,
*      paqueteServidor_t* pMemComp)
*
*\brief Función que permite actualizar la memoria compartida con los nuevos
*       valores de las alertas.
*
*\param[in]      alertasConfiguradas (alertas_t) Información de las alertas
*                                                configuradas.
*\param[in]      semId    (int) ID del semáforo de la memoria compartida.
*\param[in, out] pMemComp (paqueteServidor_t*)   Puntero al inicio de la memoria
*                                                compartida.
*
*\return void.
*/
void ActualizarAlertas(alertas_t, int, paqueteServidor_t*);

/*!
*\fn void AnalizarAlertas(
*      paqueteServidor_t *pPaqSer,
*      int semId,
*      paqueteServidor_t* pMemComp)
*
*\brief Función que primero actualiza el valor de las alertas en el
*       paqueteServidor, apuntado por "pPaqSer", según lo que esté escrito en la
*       memoria compartida. Luego se analiza si dichas alertas están encendidas
*       o no, para así terminar actualizando sus estados en el susodicho
*       paqueteServidor.
*
*\param[in, out] pPaqSer  (paqueteServidor_t*) Puntero al paqueteServidor.
*\param[in]      semId    (int) ID del semáforo de la memoria compartida.
*\param[in, out] pMemComp (paqueteServidor_t*) Puntero al inicio de la memoria
*                                              compartida.
*
*\return void.
*/
void AnalizarAlertas(paqueteServidor_t*, int, paqueteServidor_t*);

/*!
\fn void ActualizarMemoria(
*     paqueteServidor_t paqSer,
*     int semId,
*     paqueteServidor_t* pMemComp)
*
*\brief Función que actualiza el estado eléctrico y el de las alertas en la
*       memoria compartida según "paqSer". Los valores de las alertas no son
*       modificados.
*
*\param[in]      paqSer  (paqueteServidor_t)   paqueteServidor con información
*                                              de las alertas.
*\param[in]      semId    (int) ID del semáforo de la memoria compartida.
*\param[in, out] pMemComp (paqueteServidor_t*) Puntero al inicio de la memoria
*                                              compartida.
*
*\return void.
*/
void ActualizarMemoria(paqueteServidor_t, int, paqueteServidor_t*);

/*!
*\fn void EnviarEstadoAlCliente(
*      int newClientTCPSocket,
*      int semId,
*      paqueteServidor_t* pMemComp
*      errorPrograma_t* pEstProg)
*
*\brief Función que envía el estado eléctrico y de las alertas al cliente.
*
*\details Accede a la memoria compartida de forma segura por medio del semáforo
*         con ID "semId". Actualiza el valor del estado del programa, por medio
*         del puntero "pEstProg", en caso de que suceda algún error.
*
*\param[in]      newClientTCPSocket (int) FD del socket TCP del cliente.
*\param[in]      semId    (int) ID del semáforo de la memoria compartida.
*\param[in, out] pMemComp (paqueteServidor_t*) Puntero al inicio de la memoria
*                                              compartida.
*\param[in, out] pEstProg (errorPrograma_t*)   Puntero al estado del programa.
*
*\return void.
*/
void EnviarEstadoAlCliente(int, int, paqueteServidor_t*, errorPrograma_t*);
#endif //CALCULOS_H
/*!
*}@
*/
