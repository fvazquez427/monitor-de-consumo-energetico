/*!
*\addtogroup cliente
*@{
*/

/*!
*\file cliente.h
*
*\date 03/02/2021
*\version 2.0
*\author Federico Alejandro Vazquez Saraullo
*
*\brief Archivo cabecera que tiene macros y declaración de tipos de datos
*       utilizados por el main y otras funciones del cliente.
*/

#ifndef CLIENTE_H
#define CLIENTE_H

/*Macros*/
/*!
*\def   ERROR_SEND_TCP
*\brief Valor retornado por send si ocurrió algún error.
*/
#define ERROR_SEND_TCP -1

/*!
*\def   STDIN_FD
*\brief Valor del file descriptor de stdin.
*/
#define STDIN_FD 0

/*!
*\def   STDIN_BUFFER_SIZE
*\brief Tamaño del buffer donde se almacenan strings ingresados por teclado.
*/
#define STDIN_BUFFER_SIZE 20

/*!
*\def   TIMEOUT_SEC
*\brief Parte en segundos del timeout para el select.
*
*\pre El timeout (segundos + microsegundos) debe ser mayor o igual al
*     TIMEOUT_HIJO_SEC del servidor. De esta forma se le da tiempo al hijo de
*     este, que atiende la conexión, a que actualice el estado actual de la
*     carga.
*/
#define TIMEOUT_SEC 1

/*!
*\def   TIMEOUT_USEC
*\brief Parte en microsegundos del timeout para el select.
*
*\pre El timeout (segundos + microsegundos) debe ser mayor o igual al
*     TIMEOUT_HIJO_SEC del servidor. De esta forma se le da tiempo al hijo de
*     este, que atiende la conexión, a que actualice el estado actual de la
*     carga.
*/
#define TIMEOUT_USEC 0

/*!
*\def   DEMORA_S
*\brief Cantidad de segundos que dura una demora. Se la usa para configurar el
*       tiempo durante el que aparecen mensajes en pantalla, antes de que esta
*       se actualice.
*
*\note Se recomienda que al menos valga 5 para que el usuario pueda leer todos
*      los mensajes.
*/
#define DEMORA_S 5

/*!
*\def   HABILITAR_PEDIDOS
*\brief Macro que habilita el envío de pedidos al servidor.
*
*\details Se usa esta macro para debugging. Al no comentarla se habilita en
*         envío de pedidos al servidor. Permite verificar que funcione el
*         control por keepalive en el servidor.
*/
#define HABILITAR_PEDIDOS

/*Tipos de datos globales*/
/*!
*\enum  bandera_t
*\brief Enumeración que se comporta como una bandera (dos posibles estados).
*/
typedef enum{
  BANDERA_BAJA = 0, /**< La bandera está baja.*/
  BANDERA_ALTA      /**< La bandera está alta o izada.*/
}bandera_t;

/*!
*\struct estado_t
*\brief  Estructura que almacena la información del estado eléctrico de la
*        carga.
*/
typedef struct{
  /*!
  *\var   corriente_mA
  *\brief Corriente de la carga en miliampere.
  */
  float corriente_mA;

  /*!
  *\var   tensionBus_V
  *\brief Tensión de bus de la carga en Volt.
  *
  *\note Que sea "de bus" implica que es la medida en el pin "Vin-" del INA219.
  */
  float tensionBus_V;

  /*!
  *\var   potencia_mW
  *\brief Potencia de la carga en miliwatt.
  */
  float potencia_mW;

  /*!
  *\var   energia_Wh
  *\brief Energía consumida por la carga en Watt Hora.
  */
  float energia_Wh;

  /*!
  *\var   energia_J
  *\brief Energía consumida por la carga en Joule.
  */
  float energia_J;
}estado_t;

/*!
*\enum  estadoAlerta_t
*\brief Enumeración que se comporta como una alerta (dos posibles estados).
*/
typedef enum{
  ALERTA_APAGADA = 0, /**< La alerta está apagada.*/
  ALERTA_ENCENDIDA    /**< La alerta está encendida.*/
}estadoAlerta_t;

/*!
*\struct alertaMaxI_t
*\brief  Información de la alerta por máxima corriente en la carga.
*/
typedef struct{
  /*!
  *\var   alertaMaxIVal
  *\brief Valor del límite asociado a la alerta (en mA).
  */
  float alertaMaxIVal;

  /*!
  *\var   alertaMaxIEst
  *\brief Estado de la alerta (apagada o encendida).
  */
  estadoAlerta_t alertaMaxIEst;
}alertaMaxI_t;

/*!
*\struct alertaMinI_t
*\brief  Información de la alerta por mínima corriente en la carga.
*/
typedef struct{
  /*!
  *\var   alertaMinIVal
  *\brief Valor del límite asociado a la alerta (en mA).
  */
  float alertaMinIVal;

  /*!
  *\var   alertaMinIEst
  *\brief Estado de la alerta (apagada o encendida).
  */
  estadoAlerta_t alertaMinIEst;
}alertaMinI_t;

/*!
*\struct alertaMaxV_t
*\brief  Información de la alerta por máxima tensión de bus en la carga.
*/
typedef struct{
  /*!
  *\var   alertaMaxVVal
  *\brief Valor del límite asociado a la alerta (en V).
  */
  float alertaMaxVVal;

  /*!
  *\var   alertaMaxVEst
  *\brief Estado de la alerta (apagada o encendida).
  */
  estadoAlerta_t alertaMaxVEst;
}alertaMaxV_t;

/*!
*\struct alertaMinV_t
*\brief  Información de la alerta por mínima tensión de bus en la carga.
*/
typedef struct{
  /*!
  *\var   alertaMinVVal
  *\brief Valor del límite asociado a la alerta (en V).
  */
  float alertaMinVVal;

  /*!
  *\var   alertaMinVEst
  *\brief Estado de la alerta (apagada o encendida).
  */
  estadoAlerta_t alertaMinVEst;
}alertaMinV_t;

/*!
*\struct alertaMaxP_t
*\brief  Información de la alerta por máxima potencia en la carga.
*/
typedef struct{
  /*!
  *\var   alertaMaxPVal
  *\brief Valor del límite asociado a la alerta (en mW).
  */
  float alertaMaxPVal;

  /*!
  *\var   alertaMaxPEst
  *\brief Estado de la alerta (apagada o encendida).
  */
  estadoAlerta_t alertaMaxPEst;
}alertaMaxP_t;

/*!
*\struct alertaMinP_t
*\brief  Información de la alerta por mínima potencia en la carga.
*/
typedef struct{
  /*!
  *\var   alertaMinPVal
  *\brief Valor del límite asociado a la alerta (en mW).
  */
  float alertaMinPVal;

  /*!
  *\var   alertaMinPEst
  *\brief Estado de la alerta (apagada o encendida).
  */
  estadoAlerta_t alertaMinPEst;
}alertaMinP_t;

/*!
*\struct alertaMaxCon_t
*\brief  Información de la alerta por máximo consumo energético en la carga.
*/
typedef struct{
  /*!
  *\var   alertaMaxConValWh
  *\brief Valor del límite asociado a la alerta (en mW).
  */
  float alertaMaxConValWh;

  /*!
  *\var   alertaMaxConValJ
  *\brief Valor del límite asociado a la alerta (en J).
  */
  float alertaMaxConValJ;

  /*!
  *\var   alertaMaxConEst
  *\brief Estado de la alerta (apagada o encendida).
  */
  estadoAlerta_t alertaMaxConEst;
}alertaMaxCon_t;

/*!
*\struct alertaMinCon_t
*\brief  Información de la alerta por mínimo consumo energético en la carga.
*/
typedef struct{
  /*!
  *\var   alertaMinConValWh
  *\brief Valor del límite asociado a la alerta (en mW).
  */
  float alertaMinConValWh;

  /*!
  *\var   alertaMinConValJ
  *\brief Valor del límite asociado a la alerta (en J).
  */
  float alertaMinConValJ;

  /*!
  *\var   alertaMinConEst
  *\brief Estado de la alerta (apagada o encendida).
  */
  estadoAlerta_t alertaMinConEst;
}alertaMinCon_t;

/*!
*\struct alertas_t
*\brief  Información del valor y estado de todas las alertas de la carga.
*/
typedef struct{
  /*!
  *\var   alMaxI
  *\brief Información de la alerta por máxima corriente en la carga.
  */
  alertaMaxI_t   alMaxI;

  /*!
  *\var   alMinI
  *\brief Información de la alerta por mínima corriente en la carga.
  */
  alertaMinI_t   alMinI;

  /*!
  *\var   alMaxV
  *\brief Información de la alerta por máxima tensión de bus en la carga.
  */
  alertaMaxV_t   alMaxV;

  /*!
  *\var   alMinV
  *\brief Información de la alerta por mínima tensión de bus en la carga.
  */
  alertaMinV_t   alMinV;

  /*!
  *\var   alMaxP
  *\brief Información de la alerta por máxima potencia en la carga.
  */
  alertaMaxP_t   alMaxP;

  /*!
  *\var   alMinP
  *\brief Información de la alerta por mínima potencia en la carga.
  */
  alertaMinP_t   alMinP;

  /*!
  *\var   alMaxCon
  *\brief Información de la alerta por máximo consumo energético en la carga.
  */
  alertaMaxCon_t alMaxCon;

  /*!
  *\var   alMinCon
  *\brief Información de la alerta por mínimo consumo energético en la carga.
  */
  alertaMinCon_t alMinCon;
}alertas_t;

/*!
*\struct paqueteServidor_t
*\brief  Paquete de información enviado por el servidor, el cual contiene
*        el estado eléctrico actual de la carga y el estado actual de todas sus
*        alertas.
*/
typedef struct{
  /*!
  *\var   estad
  *\brief Estado eléctrico actual de la carga.
  */
  estado_t estad;

  /*!
  *\var   alert
  *\brief Estado actual de todas las alertas de la carga.
  */
  alertas_t alert;
}paqueteServidor_t;

/*!
*\enum  pedidoEstado_t
*\brief Pedido afirmativo o negativo del estado eléctrico actual de la carga.
*/
typedef enum{
  PEDIR_ESTADO_NO = 0, /**< SI se pide el estado eléctrico actual.*/
  PEDIR_ESTADO_SI      /**< NO se pide el estado eléctrico actual.*/
}pedidoEstado_t;

/*!
*\enum  pedidoAlertas_t
*\brief Pedido afirmativo o negativo de configuración de las alertas.
*/
typedef enum{
  CAMBIAR_ALERTAS_NO = 0, /**< SI se pide configurar las alertas.*/
  CAMBIAR_ALERTAS_SI      /**< NO se pide configurar las alertas.*/
}pedidoAlertas_t;

/*!
*\struct pedidoDelCliente_t
*\brief  Paquete enviado por el cliente al servidor para iniciar un pedido, sea
*        del estado eléctrico actual de la carga y/o de configuración de las
*        alertas.
*/
typedef struct{
  /*!
  *\var   pedEstado
  *\brief Pedido afirmativo o negativo del estado eléctrico actual de la carga.
  */
  pedidoEstado_t pedEstado;

  /*!
  *\var   pedAlertas
  *\brief Pedido afirmativo o negativo de la configuración de las alertas.
  */
  pedidoAlertas_t pedAlertas;
}pedidoDelCliente_t;

/*!
*\enum  mensajeDelServidor_t
*\brief Pedido de información que hace el servidor en la comunicación con el
*       cliente.
*/
typedef enum{
  SERVIDOR_PIDE_CONTRASENIA = 0,         /**< Se pide la contraseña para
                                         configurar las alertas.*/
  SERVIDOR_PIDE_CONFIGURACION,           /**< Se pide la información de
                                         configuración de las alertas.*/
  SERVIDOR_AVISA_CONTRASENIA_INCORRECTA, /**< El servidor avisa que la
                                         contraseña ingresada es incorrecta.*/
  SERVIDOR_AVISA_CONFIGURACION_HECHA     /**< El servidor avisa que la
                                         configuración de las alertas se hizo
                                         correctamente.*/
}mensajeDelServidor_t;

#endif //CLIENTE_H
/*!
*}@
*/
