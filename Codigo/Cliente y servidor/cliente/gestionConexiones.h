/*!
*\addtogroup cliente
*@{
*/

/*!
*\file gestionConexiones.h
*
*\date 03/02/2021
*\version 2.0
*\author Federico Alejandro Vazquez Saraullo
*
*\brief Archivo cabecera que tiene macros, declaración de tipos de datos y
*       prototipos de funciones para inicializar y terminar al cliente, y para
*       enviar distintos mensajes al servidor.
*/

#ifndef GESTION_CONEXIONES_H
#define GESTION_CONEXIONES_H

/*Inclusiones de bibliotecas*/
#include "cliente.h" /*pedidoEstado_t, pedidoAlertas_t, alertas_t y para
                      *gestionConexiones.c.*/
/*Macros*/
/*!
*\def   CANT_MAIN_ARG
*\brief Cantidad de argumento del main.
*/
#define CANT_MAIN_ARG 2

/*!
*\def   ERROR_SOCKET
*\brief Valor retornado por socket si ocurrió algún error.
*/
#define ERROR_SOCKET -1

/*!
*\def   ERROR_CONNECT
*\brief Valor retornado por connect si ocurrió algún error.
*/
#define ERROR_CONNECT -1

/*!
*\def   SERVIDOR_PUERTO_TCP
*\brief Puerto TCP del servidor.
*/
#define SERVIDOR_PUERTO_TCP 10000

/*Tipos de datos globales*/

/*!
*\enum errorPrograma_t
*\brief  Enumeración que engloba los posibles errores por los que termina el
*        programa del cliente.
*/
typedef enum{
	SIN_ERROR = 0,         /**< No hubo errores. */
	ERROR_ARGUMENTOS_MAIN, /**< Cantidad incorrecta de argumentos del main. */
	ERROR_GETHOSTBYNAME,   /**< Error al ejecutar la función gethostbyname. */
	ERROR_CREAR_SOCKET,    /**< Error al crear el socket. */
	ERROR_HACER_CONNECT    /**< Error al ejecutar la función connect.*/
}errorPrograma_t;

/*Prototipos de funciones*/

/*!
*\fn errorPrograma_t IniciarCliente(
*     int  argc,
*     char *argv[],
*     int  *pClientTCPSocket)
*
*\brief Función que inicializa al cliente.
*
*\details Esta función hace en orden lo siguiente: verifica la cantidad de
*         argumentos por el main, obtiene la IP por el nombre del host (si es
*         que se proporcionó este y no su IP), crea el socket e intenta
*         conectarse con el servidor.
*           Retorna un código de error para notificar si sucedió o no algún
*         error.
*
*\param[in]       argc (int)    Cantidad de argumentos recibidos por el main.
*\param[in, out]  argv (char**) Vector de strings de los argumentos del main.
*\param[in, out]  pClientTCPSocket (int*) Puntero al socket TCP del cliente.
*
*\return errorPrograma_t con el código del error que sucedió (si es que hubo
*        alguno).
*/
errorPrograma_t IniciarCliente(int, char **, int *);

/*!
*\fn int EnviarPedidoAlServidor(
*      int             clientTCPSocket,
*      pedidoEstado_t  pedidoEstado,
*      pedidoAlertas_t pedidoAlertas)
*
*\brief Función que permite enviar pedidos al servidor, sean del estado actual
*       y/o para configurar las alertas.
*
*\param[in] clientTCPSocket (int)             Socket TCP del cliente.
*\param[in] pedidoEstado    (pedidoEstado_t)  Pedido o no del estado actual.
*\param[in] pedidoAlertas   (pedidoAlertas_t) Pedido o no para configurar las
*                                             alertas.
*
*\return int que vale la cantidad de bytes enviados o -1 si hubo algún error.
*
*\warning Por el momento el servidor solo permite que el cliente pida una cosa a
*         la vez. En otras palabras, si pide el estado actual de la carga o
*         configurar las alertas, pero no los dos al mismo tiempo.
*/
int EnviarPedidoAlServidor(int, pedidoEstado_t, pedidoAlertas_t);

/*!
*\fn int EnviarContraseniaAlServidor(
*      int  clientTCPSocket,
*      char contrasenia[],
*      int  tamContrasenia)
*
*\brief Función que permite enviar la contraseña al servidor para configurar las
*       alertas en este.
*
*\param[in]      clientTCPSocket (int)   Socket TCP del cliente.
*\param[in, out] contrasenia     (char*) String con contraseña a enviar.
*\param[in]      tamContrasenia  (int)   Tamaño en bytes de la contraseña
*                                        (INCLUYE al fin de cadena '\0').
*
*\return int que vale la cantidad de bytes enviados o -1 si hubo algún error.
*
*\pre Para que la comunicación sea correcta, antes de enviar la contraseña al
*     servidor se debe recibir el pedido de la misma por parte de este.
*/
int EnviarContraseniaAlServidor(int, char [], int);

/*!
*\fn int EnviarConfiguracionAlServidor(
*      int       clientTCPSocket,
*      alertas_t alertasConfiguradas)
*
*\brief Función que permite enviar las alertas configuradas al servidor.
*
*\param[in] clientTCPSocket     (int)       Socket TCP del cliente.
*\param[in] alertasConfiguradas (alertas_t) Alertas configuradas.
*
*\return int que vale la cantidad de bytes enviados o -1 si hubo algún error.
*
*\warning El ESTADO de cada alerta es irrelevante, solo importa su valor para
*         la configuración.
*/
int EnviarConfiguracionAlServidor(int, alertas_t);

/*!
*\fn void TerminarCliente(int clientTCPSocket)
*
*\brief Función que finaliza o devuelve recursos del cliente. Para esta versión,
*        solo se cierra el socket clientTCPSocket.
*
*\param[in] clientTCPSocket (int) Socket TCP del cliente.
*
*\return void.
*/
void TerminarCliente(int);
#endif //GESTION_CONEXIONES_H
/*!
*}@
*/
