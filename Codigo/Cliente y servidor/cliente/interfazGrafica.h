/*!
*\addtogroup cliente
*@{
*/

/*!
*\file interfazGrafica.h
*
*\date 03/02/2021
*\version 2.0
*\author Federico Alejandro Vazquez Saraullo
*
*\brief Archivo cabecera que tiene macros y prototipos de funciones relacionados
*       con la interfaz gráfica del cliente.
*/

#ifndef INTERFAZ_GRAFICA_H
#define INTERFAZ_GRAFICA_H

/*Inclusiones de bibliotecas*/
#include "cliente.h" //paqueteServidor_t, alertas_t y para interfazGrafica.c.

/*Macros*/
/*!
*\def     LIMITE_MAX_I
*\brief   Corriente máxima posible a medir.
*
*\warning Esta expresada en miliampere (mA).
*\note    Este valor se deriva del límite del módulo con IC INA219.
*/
#define LIMITE_MAX_I 3200

/*!
*\def     LIMITE_MIN_I
*\brief   Corriente mínima posible a medir.
*
*\warning Esta expresada en miliampere (mA).
*\note    Este valor se deriva del límite del módulo con IC INA219.
*/
#define LIMITE_MIN_I -3200

/*!
*\def     LIMITE_MAX_V
*\brief   Tensión máxima posible a medir.
*
*\warning Esta expresada en Volt (V).
*\note    Este valor se deriva del límite del módulo con IC INA219.
*/
#define LIMITE_MAX_V 26

/*!
*\def     LIMITE_MIN_V
*\brief   Tensión mínima posible a medir.
*
*\warning Esta expresada en Volt (V).
*\note    Este valor se deriva del límite del módulo con IC INA219.
*/
#define LIMITE_MIN_V 0

/*!
*\def     LIMITE_MAX_P
*\brief   Potencia máxima posible a medir.
*
*\warning Esta expresada en miliwatt (mW).
*\note    Este valor se deriva del límite del módulo con IC INA219, calculado
*         como LIMITE_MAX_I * LIMITE_MAX_V.
*/
#define LIMITE_MAX_P 83200

/*!
*\def     LIMITE_MIN_P
*\brief   Potencia mínima posible a medir.
*
*\warning Esta expresada en miliwatt (mW).
*\note    Este valor se deriva del límite del módulo con IC INA219, calculado
*         como LIMITE_MIN_I * LIMITE_MAX_V.
*/
#define LIMITE_MIN_P -83200

/*!
*\def     CONSTANTE_WH_A_J
*\brief   Constante para pasar de Watt Hora (Wh) a Joule (J).
*
*\warning Esta expresada en Watt Hora por Joule (Wh/J).
*/
#define CONSTANTE_WH_A_J 3600

/*Prototipos de funciones*/

/*!
*\fn void ActualizarPantalla (paqueteServidor_t paqueteServidor)
*
*\brief Función que permite actualizar lo mostrado en pantalla.
*
*\details Específicamente, esta función borra la pantalla y vuelve a imprimir
*         la información del paqueteServidor: estado actual de la carga más
*         el estado de las alertas.
*
*\param[in] paqueteServidor (paqueteServidor_t) Paquete recibido del servidor.
*
*\return    void.
*/
void ActualizarPantalla (paqueteServidor_t);

/*!
*\fn void MostrarMenuOpciones()
*
*\brief Función que muestra por pantalla el menú de opciones.
*
*\return void.
*/
void MostrarMenuOpciones();

/*!
*\fn void PedirContrasenia(char buffer[])
*
*\brief Función que permite ingresar por pantalla la contraseña para
*       configurar las alertas.
*
*\return void.
*/
void PedirContrasenia(char[]);

/*!
*\fn alertas_t MostrarMenuConfiguracion(alertas_t alertasActuales)
*
*\brief Función que muestra el menú de configuración de las alertas y permite
*       configurar el valor de cada una.
*
*\details La función retorna las alertas configuradas (su estado no importa
*         mucho, aunque por seguridad se las pone como apagadas). Se le pasa
*         la configuración actual de las alertas por argumento para que el
*         usuario sepa como están configuradas.
*           Al configurar los valores se valida que no se superen los límites
*         mínimos y máximos.
*
*\warning La configuración actual que se muestra es en base a la última
*         recibida por el cliente. Esta puede no corresponderse con la real
*         porque durante la configuración otro cliente pudo haber realizado
*         correctamente la modificación de las alertas. Es por esto que cada
*         cliente DEBE configurar todas las alertas y no solo las que desee,
*         de forma de asegurarse que TODAS las alertas queden como quiera.
*
*\param[in] alertasActuales (alertas_t) Configuración actual de las alertas.
*
*\return    alertas_t con los valores configurados de las alertas (los estados
*           son irrelevantes).
*/
alertas_t MostrarMenuConfiguracion(alertas_t);
#endif //INTERFAZ_GRAFICA_H
/*!
*}@
*/
