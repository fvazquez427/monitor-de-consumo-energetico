/*!
*\addtogroup cliente
*@{
*/

/*!
*\file interfazGrafica.c
*
*\date 03/02/2021
*\version 2.0
*\author Federico Alejandro Vazquez Saraullo
*
*\brief Archivo fuente que contiene las implementaciones de las funciones
*       para manejar la interfaz gráfica del cliente.
*/

/*Inclusiones de bibliotecas*/
#include <stdio.h>  //printf, scanf y getchar.
#include <stdlib.h> //system.
#include <string.h> //strcasecmp.

#include "interfazGrafica.h" //Por ser su archivo cabecera.
#include "manejoStrings.h"   //LeerTexto.

/*Desarrollo de funciones*/
void ActualizarPantalla (paqueteServidor_t paqueteServidor)
{
  /*Cuerpo de la función*/
  system("clear");

  //Estado actual.
  printf("\n\t\tCorriente (mA): %.3f"
         "\n\t\tTension del bus (V): %.3f"
         "\n\t\tPotencia (mW): %.3f"
         "\n\t\tEnergia (Wh): %.3f"
         "\n\t\tEnergia (J): %.3f\n",
         paqueteServidor.estad.corriente_mA,
         paqueteServidor.estad.tensionBus_V,
         paqueteServidor.estad.potencia_mW,
         paqueteServidor.estad.energia_Wh,
         paqueteServidor.estad.energia_J);

  putchar('\n');

  //Estado de las alertas.
  if(paqueteServidor.alert.alMaxI.alertaMaxIEst == ALERTA_ENCENDIDA){
    printf("\n\t**ATENCION: Alerta por EXCESO DE CORRIENTE** -> "
           "La corriente llego o supero los %.3f mA.\n",
           paqueteServidor.alert.alMaxI.alertaMaxIVal);
  }

  if(paqueteServidor.alert.alMinI.alertaMinIEst == ALERTA_ENCENDIDA){
    printf("\n\t**ATENCION: Alerta por FALTA DE CORRIENTE** -> "
           "La corriente llego o descendio por debajo de los %.3f mA.\n",
           paqueteServidor.alert.alMinI.alertaMinIVal);
  }

  if(paqueteServidor.alert.alMaxV.alertaMaxVEst == ALERTA_ENCENDIDA){
    printf("\n\t**ATENCION: Alerta por EXCESO DE TENSION** -> "
           "La tension llego o supero los %.3f V.\n",
           paqueteServidor.alert.alMaxV.alertaMaxVVal);
  }

  if(paqueteServidor.alert.alMinV.alertaMinVEst == ALERTA_ENCENDIDA){
    printf("\n\t**ATENCION: Alerta por FALTA DE TENSION** -> "
           "La tension llego o descendio por debajo de los %.3f V.\n",
           paqueteServidor.alert.alMinV.alertaMinVVal);
  }

  if(paqueteServidor.alert.alMaxP.alertaMaxPEst == ALERTA_ENCENDIDA){
    printf("\n\t**ATENCION: Alerta por EXCESO DE POTENCIA** -> "
           "La potencia llego o supero los %.3f mW.\n",
           paqueteServidor.alert.alMaxP.alertaMaxPVal);
  }

  if(paqueteServidor.alert.alMinP.alertaMinPEst == ALERTA_ENCENDIDA){
    printf("\n\t**ATENCION: Alerta por FALTA DE POTENCIA** -> "
           "La potencia llego o descendio por debajo de los %.3f mW.\n",
           paqueteServidor.alert.alMinP.alertaMinPVal);
  }

  if(paqueteServidor.alert.alMaxCon.alertaMaxConEst == ALERTA_ENCENDIDA){
    printf("\n\t**ATENCION: Alerta por EXCESO DE CONSUMO DE ENERGIA** -> "
           "El consumo llego o supero los %.3f Wh o %.3f J.\n",
           paqueteServidor.alert.alMaxCon.alertaMaxConValWh,
           paqueteServidor.alert.alMaxCon.alertaMaxConValJ);
  }

  if(paqueteServidor.alert.alMinCon.alertaMinConEst == ALERTA_ENCENDIDA){
    printf("\n\t**ATENCION: Alerta por FALTA DE CONSUMO DE ENERGIA** -> "
           "El consumo llego o descendio por debajo de los %.3f Wh o %.3f J.\n",
           paqueteServidor.alert.alMinCon.alertaMinConValWh,
           paqueteServidor.alert.alMinCon.alertaMinConValJ);
  }

  printf("\n\nPresione una vez ENTER para ingresar al menu de "
         "opciones.\n");
}

void MostrarMenuOpciones()
{
  /*Cuerpo de la función*/
  printf("\nEscriba una opcion (\"fin\" o \"configurar\"): ");
}

void PedirContrasenia(char buffer[])
{
  /*Cuerpo de la función*/
  printf("\nIngrese la contrasenia: ");
  LeerTexto(buffer, STDIN_BUFFER_SIZE);
}

alertas_t MostrarMenuConfiguracion(alertas_t alertasActuales)
{
  /*Declaraciones locales*/
  alertas_t alertasConfiguradas;
  char buffer[STDIN_BUFFER_SIZE];

  /*Cuerpo de la función*/
  //Se muestra la configuración actual.
  printf("\n\n---CONFIGURACION ACTUAL DE ALERTAS---\n\n"
         "Alerta por corriente maxima = %.3f mA.\n"
         "Alerta por corriente minima = %.3f mA.\n"
         "Alerta por tension maxima = %.3f V.\n"
         "Alerta por tension minima = %.3f V.\n"
         "Alerta por potencia maxima = %.3f mW.\n"
         "Alerta por potencia minima = %.3f mW.\n"
         "Alerta por consumo energetico maximo = %.3f Wh o %.3f J.\n"
         "Alerta por consumo energetico minimo = %.3f Wh o %.3f J.\n",
         alertasActuales.alMaxI.alertaMaxIVal,
         alertasActuales.alMinI.alertaMinIVal,
         alertasActuales.alMaxV.alertaMaxVVal,
         alertasActuales.alMinV.alertaMinVVal,
         alertasActuales.alMaxP.alertaMaxPVal,
         alertasActuales.alMinP.alertaMinPVal,
         alertasActuales.alMaxCon.alertaMaxConValWh,
         alertasActuales.alMaxCon.alertaMaxConValJ,
         alertasActuales.alMinCon.alertaMinConValWh,
         alertasActuales.alMinCon.alertaMinConValJ);

  //Se configuran las alertas. Se las pone primero como apagadas por las dudas.
  alertasConfiguradas.alMaxI.alertaMaxIEst = ALERTA_APAGADA;
  alertasConfiguradas.alMinI.alertaMinIEst = ALERTA_APAGADA;
  alertasConfiguradas.alMaxV.alertaMaxVEst = ALERTA_APAGADA;
  alertasConfiguradas.alMinV.alertaMinVEst = ALERTA_APAGADA;
  alertasConfiguradas.alMaxP.alertaMaxPEst = ALERTA_APAGADA;
  alertasConfiguradas.alMinP.alertaMinPEst = ALERTA_APAGADA;
  alertasConfiguradas.alMaxCon.alertaMaxConEst = ALERTA_APAGADA;
  alertasConfiguradas.alMinCon.alertaMinConEst = ALERTA_APAGADA;

  //Corriente máxima.
  printf("\nIngrese el valor de la CORRIENTE MAXIMA en mA "
         "(mayor que %d mA pero con %d mA como maximo): ",
         LIMITE_MIN_I, LIMITE_MAX_I);
  scanf("%f", &(alertasConfiguradas.alMaxI.alertaMaxIVal));

  while( (alertasConfiguradas.alMaxI.alertaMaxIVal > LIMITE_MAX_I) || \
         (alertasConfiguradas.alMaxI.alertaMaxIVal <= LIMITE_MIN_I)){
    printf("\nERROR: Valor INVALIDO.\nReingrese el valor de la CORRIENTE "
           "MAXIMA en mA (mayor que %d mA pero con %d mA como maximo): ",
           LIMITE_MIN_I, LIMITE_MAX_I);
    scanf("%f", &(alertasConfiguradas.alMaxI.alertaMaxIVal));
  }

  //Corriente mínima.
  printf("\n\nIngrese el valor de la CORRIENTE MINIMA en mA "
         "(%d mA como minimo pero menor que %.3f mA): ",
         LIMITE_MIN_I, alertasConfiguradas.alMaxI.alertaMaxIVal);
  scanf("%f", &(alertasConfiguradas.alMinI.alertaMinIVal));

  while( (alertasConfiguradas.alMinI.alertaMinIVal < LIMITE_MIN_I) || \
         (alertasConfiguradas.alMinI.alertaMinIVal >= \
           alertasConfiguradas.alMaxI.alertaMaxIVal)){
    printf("\nERROR: Valor INVALIDO.\nReingrese el valor de la CORRIENTE "
           "MINIMA en mA (%d mA como minimo pero menor que %.3f mA): ",
           LIMITE_MIN_I, alertasConfiguradas.alMaxI.alertaMaxIVal);
    scanf("%f", &(alertasConfiguradas.alMinI.alertaMinIVal));
  }

  //Tensión máxima.
  printf("\n\nIngrese el valor de la TENSION MAXIMA en V "
         "(mayor que %d V pero con %d V como maximo): ",
         LIMITE_MIN_V, LIMITE_MAX_V);
  scanf("%f", &(alertasConfiguradas.alMaxV.alertaMaxVVal));

  while( (alertasConfiguradas.alMaxV.alertaMaxVVal > LIMITE_MAX_V) || \
         (alertasConfiguradas.alMaxV.alertaMaxVVal <= LIMITE_MIN_V)){
    printf("\nERROR: Valor INVALIDO.\nReingrese el valor de la TENSION "
           "MAXIMA en V (mayor que %d V pero con %d V como maximo): ",
           LIMITE_MIN_V, LIMITE_MAX_V);
    scanf("%f", &(alertasConfiguradas.alMaxV.alertaMaxVVal));
  }

  //Tensión mínima.
  printf("\n\nIngrese el valor de la TENSIÓN MINIMA en V "
         "(%d V como minimo pero menor que %.3f V): ",
         LIMITE_MIN_V, alertasConfiguradas.alMaxV.alertaMaxVVal);
  scanf("%f", &(alertasConfiguradas.alMinV.alertaMinVVal));

  while( (alertasConfiguradas.alMinV.alertaMinVVal < LIMITE_MIN_V) || \
         (alertasConfiguradas.alMinV.alertaMinVVal >= \
           alertasConfiguradas.alMaxV.alertaMaxVVal)){
    printf("\nERROR: Valor INVALIDO.\nReingrese el valor de la TENSIÓN "
           "MINIMA en V (%d V como minimo pero menor que %.3f V): ",
           LIMITE_MIN_V, alertasConfiguradas.alMaxV.alertaMaxVVal);
    scanf("%f", &(alertasConfiguradas.alMinV.alertaMinVVal));
  }

  //Potencia máxima.
  printf("\n\nIngrese el valor de la POTENCIA MAXIMA en mW "
         "(mayor que %d mW pero con %d mW como maximo): ",
         LIMITE_MIN_P, LIMITE_MAX_P);
  scanf("%f", &(alertasConfiguradas.alMaxP.alertaMaxPVal));

  while( (alertasConfiguradas.alMaxP.alertaMaxPVal > LIMITE_MAX_P) || \
         (alertasConfiguradas.alMaxP.alertaMaxPVal <= LIMITE_MIN_P)){
    printf("\nERROR: Valor INVALIDO.\nReingrese el valor de la POTENCIA "
           "MAXIMA en mW (mayor que %d mW pero con %d mW como maximo): ",
           LIMITE_MIN_P, LIMITE_MAX_P);
    scanf("%f", &(alertasConfiguradas.alMaxP.alertaMaxPVal));
  }

  //Potencia mínima.
  printf("\n\nIngrese el valor de la POTENCIA MINIMA en mW "
         "(%d mW como minimo pero menor que %.3f mW): ",
         LIMITE_MIN_P, alertasConfiguradas.alMaxP.alertaMaxPVal);
  scanf("%f", &(alertasConfiguradas.alMinP.alertaMinPVal));

  while( (alertasConfiguradas.alMinP.alertaMinPVal < LIMITE_MIN_P) || \
         (alertasConfiguradas.alMinP.alertaMinPVal >= \
           alertasConfiguradas.alMaxP.alertaMaxPVal)){
    printf("\nERROR: Valor INVALIDO.\nReingrese el valor de la POTENCIA "
           "MINIMA en mW (%d mW como minimo pero menor que %.3f mW): ",
           LIMITE_MIN_P, alertasConfiguradas.alMaxP.alertaMaxPVal);
    scanf("%f", &(alertasConfiguradas.alMinP.alertaMinPVal));
  }

  //Consumo energético máximo.
  printf("\n\n¿En que unidad desea ingresar el CONSUMO ENERGETICO MAXIMO?\n"
         "Ingrese \"J\" para Joules o \"Wh\" para Watt Hora: ");
  getchar(); //Limpia el buffer de teclado
  LeerTexto(buffer, STDIN_BUFFER_SIZE);

  while((strcasecmp(buffer, "J") != 0) && (strcasecmp(buffer, "Wh") != 0)){
    printf("\nUnidad INVALIDA. Ingrese \"J\" para Joules o \"Wh\" para Watt "
           "Hora: ");
    LeerTexto(buffer, STDIN_BUFFER_SIZE);
  }

  //En Joules.
  if(!(strcasecmp(buffer, "J"))){
    printf("\nIngrese el valor del CONSUMO ENERGETICO MAXIMO en J "
           "(puede ser positivo o negativo): ");
    scanf("%f", &(alertasConfiguradas.alMaxCon.alertaMaxConValJ));

    //Se calcula el consumo máximo en Wh.
    alertasConfiguradas.alMaxCon.alertaMaxConValWh = \
      alertasConfiguradas.alMaxCon.alertaMaxConValJ / CONSTANTE_WH_A_J;
  }

  //En Watt Hora.
  else if(!(strcasecmp(buffer, "Wh"))){
    printf("\nIngrese el valor del CONSUMO ENERGETICO MAXIMO en Wh "
           "(puede ser positivo o negativo): ");
    scanf("%f", &(alertasConfiguradas.alMaxCon.alertaMaxConValWh));

    //Se calcula el consumo máximo en J.
    alertasConfiguradas.alMaxCon.alertaMaxConValJ = \
      alertasConfiguradas.alMaxCon.alertaMaxConValWh * CONSTANTE_WH_A_J;
  }

  //Consumo energético mínimo.
  printf("\n\n¿En que unidad desea ingresar el CONSUMO ENERGETICO MINIMO?\n"
         "Ingrese \"J\" para Joules o \"Wh\" para Watt Hora: ");
  getchar(); //Limpia el buffer de teclado
  LeerTexto(buffer, STDIN_BUFFER_SIZE);

  while((strcasecmp(buffer, "J") != 0) && (strcasecmp(buffer, "Wh") != 0)){
    printf("\nUnidad INVALIDA. Ingrese \"J\" para Joules o \"Wh\" para Watt "
           "Hora: ");
    LeerTexto(buffer, STDIN_BUFFER_SIZE);
  }

  //En Joules.
  if(!(strcasecmp(buffer, "J"))){
    printf("\nIngrese el valor del CONSUMO ENERGETICO MINIMO en J "
           "(puede ser positivo o negativo pero debe ser menor que %.3f J): ",
           alertasConfiguradas.alMaxCon.alertaMaxConValJ);
    scanf("%f", &(alertasConfiguradas.alMinCon.alertaMinConValJ));

    while(alertasConfiguradas.alMinCon.alertaMinConValJ >= \
          alertasConfiguradas.alMaxCon.alertaMaxConValJ){
      printf("\nERROR: Valor INVALIDO.\nReingrese el valor del CONSUMO "
             "ENERGETICO MINIMO en J (puede ser positivo o negativo pero debe "
             "ser menor que %.3f J): ",
             alertasConfiguradas.alMaxCon.alertaMaxConValJ);
      scanf("%f", &(alertasConfiguradas.alMinCon.alertaMinConValJ));
    }

    //Se calcula el consumo mínimo en Wh.
    alertasConfiguradas.alMinCon.alertaMinConValWh = \
      alertasConfiguradas.alMinCon.alertaMinConValJ / CONSTANTE_WH_A_J;
  }

  //En Watt Hora.
  else if(!(strcasecmp(buffer, "Wh"))){
    printf("\nIngrese el valor del CONSUMO ENERGETICO MINIMO en Wh "
           "(puede ser positivo o negativo pero debe ser menor que %.3f Wh): ",
           alertasConfiguradas.alMaxCon.alertaMaxConValWh);
    scanf("%f", &(alertasConfiguradas.alMinCon.alertaMinConValWh));

    while(alertasConfiguradas.alMinCon.alertaMinConValWh >= \
          alertasConfiguradas.alMaxCon.alertaMaxConValWh){
      printf("\nERROR: Valor INVALIDO.\nReingrese el valor del CONSUMO "
             "ENERGETICO MINIMO en Wh (puede ser positivo o negativo pero debe "
             "ser menor que %.3f Wh): ",
             alertasConfiguradas.alMaxCon.alertaMaxConValWh);
      scanf("%f", &(alertasConfiguradas.alMinCon.alertaMinConValWh));
    }

    //Se calcula el consumo máximo en J.
    alertasConfiguradas.alMinCon.alertaMinConValJ = \
      alertasConfiguradas.alMinCon.alertaMinConValWh * CONSTANTE_WH_A_J;
  }

  getchar(); //Se limpia el buffer de teclado.

  return alertasConfiguradas;
}
/*!
*}@
*/
