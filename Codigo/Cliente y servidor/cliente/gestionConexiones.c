/*!
*\addtogroup cliente
*@{
*/

/*!
*\file gestionConexiones.c
*
*\date 03/02/2021
*\version 2.0
*\author Federico Alejandro Vazquez Saraullo
*
*\brief Archivo fuente que contiene las implementaciones de las funciones
*       para inicializar y terminar al cliente, y para enviar distintos mensajes
*       al servidor.
*/

/*Inclusiones de bibliotecas*/
#include <stdio.h>      //fprintf.
#include <netdb.h>      //struct hostent, gethostbyname y hstrerror.
#include <errno.h>      //errno.
#include <sys/types.h>  //socket, connect y send.
#include <sys/socket.h> /*socket, connect, struct sockaddr_in, struct in_addr,
                         *struct sockaddr y send*/
#include <netinet/in.h> //struct sockaddr_in, struct in_addr y struct sockaddr.
#include <netinet/ip.h> //struct sockaddr_in, struct in_addr y struct sockaddr.
#include <arpa/inet.h>  //htons.
#include <string.h>     //strerror.
#include <strings.h>    //bzero.
#include <unistd.h>     //close.

#include "gestionConexiones.h" //Por ser su archivo cabecera.

/*Desarrollo de funciones*/
errorPrograma_t IniciarCliente(
  int  argc,
  char *argv[],
  int  *pClientTCPSocket)
{
  /*Declaraciones locales*/
  errorPrograma_t estadoPrograma = SIN_ERROR;
  struct sockaddr_in serverTCPAddr;
  struct hostent *he; //Guarda la IP del host.

  /*Cuerpo de la función*/
  if (argc != CANT_MAIN_ARG){
    fprintf(stderr,"\nERROR: No se cumple con el formato de ejecucion "
           "\"./Nombre_Ejecutable Nombre_Host_o_IP\"\n\n");
    estadoPrograma = ERROR_ARGUMENTOS_MAIN;
  }
  /*Se convierte el nombre del host (servidor) a su direccion IP o directamente
   *se guarda la IP del servidor.*/
  else if ((he = gethostbyname(argv[1])) == NULL){
    fprintf(stderr,"\nERROR al ejecutar \"gethostbyname\" en el "
            "cliente: %s.\n\n", hstrerror(h_errno));
    estadoPrograma = ERROR_GETHOSTBYNAME;
  }
  //Se crea el socket TCP del cliente.
  else if (((*pClientTCPSocket) = socket(AF_INET, SOCK_STREAM, 0)) \
           == ERROR_SOCKET){
    fprintf(stderr,"\nERROR al crear el socket TCP del cliente: %s.\n\n",
            strerror(errno));
    estadoPrograma = ERROR_CREAR_SOCKET;
  }
  else{
    /*Se carga la estructura serverTCPAddr con información del servidor para
    luego poder llamar a la funcion connect(). Se configura para TCP, el puerto
    SERVIDOR_PUERTO_TCP y la IP que se suministra según la línea de comando.*/
    serverTCPAddr.sin_family = AF_INET;
    serverTCPAddr.sin_port = htons(SERVIDOR_PUERTO_TCP);
    serverTCPAddr.sin_addr = *((struct in_addr *)he->h_addr);
    bzero(&(serverTCPAddr.sin_zero), 8);

    //Se intenta conectar con el servidor (bloqueante).
    if (connect((*pClientTCPSocket), (struct sockaddr *)&serverTCPAddr,
                sizeof(struct sockaddr)) == ERROR_CONNECT){
      fprintf(stderr,"\nERROR al conectarse al socket TCP del servidor: "
              "%s.\n\n", strerror(errno));
      estadoPrograma = ERROR_HACER_CONNECT;
    }
  }

  return estadoPrograma;
}

int EnviarPedidoAlServidor(
  int             clientTCPSocket,
  pedidoEstado_t  pedidoEstado,
  pedidoAlertas_t pedidoAlertas)
{
  /*Declaraciones locales*/
  pedidoDelCliente_t pedido;
  int bytesEnviados;

  /*Cuerpo de la función*/
  //Se carga el pedido completo en "pedido" y se lo envía con send.
  pedido.pedEstado = pedidoEstado;
  pedido.pedAlertas = pedidoAlertas;

  bytesEnviados = send(clientTCPSocket, &pedido, sizeof(pedido), 0);

  return bytesEnviados;
}

int EnviarContraseniaAlServidor(
  int  clientTCPSocket,
  char contrasenia[],
  int  tamContrasenia)
{
  /*Declaraciones locales*/
  int bytesEnviados;

  /*Cuerpo de la función*/
  //Se envía la contraseña con send y se guardan la cantidad de bytes enviados.
  bytesEnviados = send(clientTCPSocket, contrasenia, tamContrasenia, 0);

  return bytesEnviados;
}

int EnviarConfiguracionAlServidor(
  int       clientTCPSocket,
  alertas_t alertasConfiguradas)
{
  /*Declaraciones locales*/
  int bytesEnviados;

  /*Cuerpo de la función*/
  /*Se envía la configuración con send y se guardan la cantidad de bytes
   *enviados.*/
  bytesEnviados = send(clientTCPSocket, &alertasConfiguradas,
                       sizeof(alertasConfiguradas), 0);

  return bytesEnviados;
}

void TerminarCliente(int clientTCPSocket)
{
  /*Cuerpo de la función*/
  close(clientTCPSocket);
}
/*!
*}@
*/
