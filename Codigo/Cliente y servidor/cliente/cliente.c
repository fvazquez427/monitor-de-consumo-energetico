/*!
*\defgroup cliente Programa cliente
*\brief    Código del cliente.
*/

/*!
*\addtogroup cliente
*@{
*/

/*!
*\mainpage notitle
*
*\section intro_sec Breve descripción
*
*Este proyecto es un monitor remoto del estado eléctrico de una carga de
*corriente continua (CC). El trabajo consiste en el desarrollo de un programa
*servidor y otro cliente. <br>
*
*El sistema funciona según el siguiente diagrama en bloques: <br>
*\image html "Sistema_Diagrama en bloques_v2.png" <br>
*
*Link a repositorio remoto del proyecto:
*https://gitlab.com/fvazquez427/monitor-de-consumo-energetico <br>
*
*\section eje_sec Ejecucion
*
*\subsection ser_subsec Servidor
*En el directorio "servidor" ejecutar desde una terminal: <br>
*<dl>
* <dt>1) make limpiar </dt>
* <dt>2) make todo </dt>
* <dt>3) ./servidor.exe /dev/ttyACM(NUMERO) </dt>
*</dl>
*
*\subsection cli_subsec Cliente
*En el directorio "cliente" ejecutar desde una terminal: <br>
*<dl>
* <dt>1) make limpiar </dt>
* <dt>2) make todo </dt>
* <dt>3) ./cliente.exe (IP/NOMBRE_DEL_SERVIDOR) </dt>
*</dl>
*
*\section acImp_subsec Aclaración importante
*Este proyecto se probó exitosamente con las siguientes distribuciones y
*versiones del sistema operativo Linux: <br>
*<dl>
* <dt>1) Ubuntu 18.04.4 LTS y versión de kernel de Linux 5.4.0-51-generic. </dt>
* <dt>2) Ubuntu 18.04.5 LTS y versión de kernel de Linux 5.4.0-62-generic. </dt>
* <dt>3) Ubuntu 20.04.1 LTS y versión de kernel de Linux 5.8.0-41-generic. </dt>
*</dl>
*/

/*!
*\file cliente.c
*
*\date 03/02/2021
*\version 2.0
*\author Federico Alejandro Vazquez Saraullo
*
*\brief Archivo fuente que contiene la función principal "main" del cliente.
*
*\details El cliente se comporta según el siguiente diagrama de flujo:
*         \image html "Cliente_Diagrama en bloques_v2.png"
*/

/*Inclusiones de bibliotecas*/
#include <stdio.h>     //printf, fprintf y getchar.
#include <stdlib.h>    //exit.
#include <unistd.h>    //select, sleep y read.
#include <sys/time.h>  //struct timeval.
#include <sys/types.h> //select, fd_set, FD_ZERO, FD_SET y FD_ISSET.
#include <string.h>    //strerror y strlen.
#include <strings.h>   //strcasecmp.
#include <errno.h>     //errno.

#include "cliente.h"           /*paqueteServidor_t, mensajeDelServidor_t,
                                *alertas_t y bandera_t.*/
#include "manejoStrings.h"     //LeerTexto.
#include "gestionConexiones.h" /*errorPrograma_t, IniciarCliente,
                                *EnviarPedidoAlServidor,
                                *EnviarContraseniaAlServidor,
                                *EnviarConfiguracionAlServidor y
                                *TerminarCliente.*/
#include "interfazGrafica.h"  /*MostrarMenuOpciones, PedirContrasenia,
                               *MostrarMenuConfiguracion y ActualizarPantalla.*/
/*Función principal*/
int main(int argc, char *argv[])
{
  /*Declaraciones locales*/
  //Para conexiones TCP.
  int clientTCPSocket;
  int numRxTCPBytes;
  paqueteServidor_t paqueteServidor;
  mensajeDelServidor_t mensajeServidor;
  alertas_t alertasConfiguradas;

  //Para los select.
  fd_set readFDs;
  struct timeval tv;

  //Para lectura del teclado.
  char buffer [STDIN_BUFFER_SIZE];
  bandera_t finalizarPrograma = BANDERA_BAJA;

  //Para control de errores del programa.
  errorPrograma_t estadoPrograma;

  /*Cuerpo de la función*/
  /*Se inicializa el cliente: Se verifica la cantidad de argumentos por el main,
   *se obtiene la IP del host por su nombre, se crea el socket y se intenta
   *conectar con el servidor. De haber un error, se termina el programa.*/
  printf("\nINICIANDO cliente y CONECTANDO con servidor...\n");
  estadoPrograma = IniciarCliente(argc, argv, &clientTCPSocket);

  if(estadoPrograma != SIN_ERROR){
    printf("\n\n\t\tFIN DEL PROGRAMA\n\n");
    exit((int) estadoPrograma);
  }

  //El cliente se inició y conectó correctamente.
  printf("\n\tCliente iniciado y conectado CORRECTAMENTE.\n\nAguarde un "
         "momento por favor...\n");
  sleep(DEMORA_S);

  //Se ejecuta el programa hasta que se reciba "fin" del teclado.
  while(finalizarPrograma == BANDERA_BAJA){
    //Seteo timeout en TIMEOUT_SEC segundos y TIMEOUT_USEC microsegundos./
    tv.tv_sec = TIMEOUT_SEC;
    tv.tv_usec = TIMEOUT_USEC;

    //Configuración del reading fd set.
    FD_ZERO(&readFDs);
    FD_SET(STDIN_FD, &readFDs);

    //Se espera con select por un ingreso de teclado.
    select(STDIN_FD + 1, &readFDs, NULL, NULL, &tv);

    /*Se analiza si se escribió "fin"  o "configurar alertas" por teclado. De
     *lo contrario, se envía un pedido de estado al servidor. IMPORTANTE: Solo
     *sucede un evento por STDIN que lo haga salir del select si se presiona
     *enter. Es por ello que se le pide al usuario primero presionar dicha
     *tecla.*/
    if (FD_ISSET(STDIN_FD, &readFDs)){
      /*Pausa para ver el menú de opciones. Se borra el enter del buffer de
       *teclado.*/
      getchar();

      //Ingreso de opción por teclado.
      MostrarMenuOpciones();
      LeerTexto(buffer, STDIN_BUFFER_SIZE);

      /*Si se recibe "fin" teclado, termina el cliente. Si se recibe
       *"configurar" se comienza con la modificación de las alertas.*/
      if(!strcasecmp(buffer, "fin")){
        finalizarPrograma = BANDERA_ALTA;
      }
      else if(!strcasecmp(buffer, "configurar")){
        /*Se envía un pedido de configuración de alertas al servidor por TCP. Si
         *no hubo problemas, se lee lo recibido.*/
        if(EnviarPedidoAlServidor(clientTCPSocket, PEDIR_ESTADO_NO,
                                  CAMBIAR_ALERTAS_SI ) == ERROR_SEND_TCP){
          fprintf(stderr, "\nERROR al enviar el pedido de configuracion de "
                 "alertas al servidor por TCP: %s.\n", strerror(errno));
          sleep(DEMORA_S);
        }
        else{
          /*Se procesa el pedido recibido del servidor, que debería ser el
           *de la contraseña. De ser correcto, el cliente pide al usuario que la
           *escriba, sino muestra un mensaje de error.*/
          numRxTCPBytes = read(clientTCPSocket, &(mensajeServidor),
                               sizeof(mensajeServidor));

          if(numRxTCPBytes < 0){
            fprintf(stderr, "\nERROR al leer el pedido de contrasenia del "
                   "servidor por el socket TCP: %s.\n", strerror(errno));
            sleep(DEMORA_S);
          }
          else if(numRxTCPBytes !=  sizeof(mensajeServidor)){
            printf("\nERROR al leer el pedido de contrasenia del servidor "
                   "por el socket TCP: No se leyeron %ld bytes, sino %d.\n",
                   sizeof(mensajeServidor), numRxTCPBytes);
            sleep(DEMORA_S);
          }
          else if(mensajeServidor != SERVIDOR_PIDE_CONTRASENIA){
            printf("\nERROR al leer el pedido de contrasenia del servidor "
                   "por el socket TCP: No se recibio este pedido.\n");
            sleep(DEMORA_S);
          }
          else{
            PedirContrasenia(buffer);
            /*Se envía la contraseña al servidor por TCP. A strlen se le suma 1
             *por el fin de cadena. Si no hubo problemas, se lee lo recibido.*/
            if(EnviarContraseniaAlServidor(clientTCPSocket, buffer,
                                           strlen(buffer)+1) == ERROR_SEND_TCP){
              fprintf(stderr, "\nERROR al enviar la contrasenia al servidor "
                     "por TCP: %s.\n", strerror(errno));
              sleep(DEMORA_S);
            }
            else{
              /*Se procesa el pedido recibido del servidor, que debería ser el
               *de la información de configuración de las alertas. De ser
               *correcto, el cliente pide al usuario que la escriba, sino
               *muestra un mensaje de error.*/
              numRxTCPBytes = read(clientTCPSocket, &(mensajeServidor),
                                   sizeof(mensajeServidor));

              if(numRxTCPBytes < 0){
                fprintf(stderr, "\nERROR al leer el pedido de informacion de "
                       "configuracion del servidor por el socket TCP: %s.\n",
                        strerror(errno));
                sleep(DEMORA_S);
              }
              else if(numRxTCPBytes !=  sizeof(mensajeServidor)){
                printf("\nERROR al leer el pedido de informacion de "
                       "configuracion del servidor por el socket TCP: No se "
                       "leyeron %ld bytes, sino %d.\n",
                       sizeof(mensajeServidor), numRxTCPBytes);
                sleep(DEMORA_S);
              }
              else if(mensajeServidor == SERVIDOR_AVISA_CONTRASENIA_INCORRECTA){
                printf("\nCONTRASENIA INCORRECTA. Vuelva a empezar.\n");
                sleep(DEMORA_S);
              }
              /*Por si se recibe otro mensaje que no sea el de contraseña
               *incorrecta y tampoco el de pedido de configuración.*/
              else if(mensajeServidor != SERVIDOR_PIDE_CONFIGURACION){
                printf("\nERROR al leer el pedido de informacion de "
                       "configuracion del servidor por el socket TCP: No se "
                       "recibio este pedido.\n");
                sleep(DEMORA_S);
              }
              else{
                /*Se muestra el menú de configuración de alertas y se ingresa
                 *una a una.*/
                alertasConfiguradas = MostrarMenuConfiguracion(paqueteServidor.alert);

                /*Se envía la configuración al servidor por TCP. Si no hubo
                 *problemas, se lee lo recibido.*/
                if(EnviarConfiguracionAlServidor(clientTCPSocket,
                                       alertasConfiguradas) == ERROR_SEND_TCP){
                  fprintf(stderr, "\nERROR al enviar la configuracion al "
                         "servidor por TCP: %s.\n", strerror(errno));
                  sleep(DEMORA_S);
                }
                else{
                  /*Se procesa el mensaje recibido del servidor, que debería
                   *ser el que confirma que la configuración de alertas se
                   *hizo. De ser así, se muestra un mensaje de éxito
                   *por pantalla, sino muestra un mensaje pero de error.*/
                  numRxTCPBytes = read(clientTCPSocket, &(mensajeServidor),
                                       sizeof(mensajeServidor));

                  if(numRxTCPBytes < 0){
                    fprintf(stderr, "\nERROR al leer la confirmacion de "
                           "configuracion del servidor por el socket TCP: "
                           "%s.\n", strerror(errno));
                    sleep(DEMORA_S);
                  }
                  else if(numRxTCPBytes !=  sizeof(mensajeServidor)){
                    printf("\nERROR al leer la confirmacion de configuracion "
                           "del servidor por el socket TCP: No se leyeron %ld "
                           "bytes, sino %d.\n",
                           sizeof(mensajeServidor), numRxTCPBytes);
                    sleep(DEMORA_S);
                  }
                  else if(mensajeServidor != SERVIDOR_AVISA_CONFIGURACION_HECHA){
                    printf("\nERROR al leer la confirmacion de configuracion "
                           "del servidor por el socket TCP: No se recibio "
                           "este mensaje.\n");
                    sleep(DEMORA_S);
                  }
                  else{
                    printf("\n¡El servidor CONFIRMA que las alertas se "
                           "configuraron CORRECTAMENTE! \nVolviendo a la "
                           "pantalla principal...\n");
                    sleep(DEMORA_S);
                  }
                }
              }
            }
          }
        }
      }
      else {
        printf("\n\nOpcion INVALIDA.\n");
        sleep(DEMORA_S);
      }
    }
    #ifdef HABILITAR_PEDIDOS
    else{
      /*Se envía un pedido del estado actual al servidor por TCP. Si no hubo
       *problemas, se lee lo recibido.*/
      if(EnviarPedidoAlServidor(clientTCPSocket, PEDIR_ESTADO_SI,
                                CAMBIAR_ALERTAS_NO ) == ERROR_SEND_TCP){
        fprintf(stderr, "\nERROR al enviar el pedido de estado actual al "
               "servidor por TCP: %s.\n", strerror(errno));
        sleep(DEMORA_S);
      }
      else{
        /*Se procesa el estado recibido y, de ser correcto, se actualiza la
         *información en pantalla.*/
        numRxTCPBytes = read(clientTCPSocket, &(paqueteServidor),
                             sizeof(paqueteServidor));

        if(numRxTCPBytes < 0){
          fprintf(stderr, "\nERROR al leer el paquete del servidor por "
                 "el socket TCP: %s.\n", strerror(errno));
          sleep(DEMORA_S);
        }
        else{
          if(numRxTCPBytes !=  sizeof(paqueteServidor)){
            printf("\nERROR al leer el paquete del servidor por el "
                   "socket TCP: No se leyeron %ld bytes, sino %d.\n",
                   sizeof(paqueteServidor), numRxTCPBytes);
            sleep(DEMORA_S);
          }
          else{
            ActualizarPantalla(paqueteServidor);
          }
        }
      }
    }
    #endif //HABILITAR_PEDIDOS
  }

  //Se cierra el socket TCP.
  TerminarCliente(clientTCPSocket);

  //Fin del programa.
  printf("\n\n\t\tFIN DEL PROGRAMA\n\n");
  return 0;
}

/*!
*}@
*/
